#include "work.h"
#include "ui_work.h"
#include "stdio.h"
#include "db_connection.h"
#include <QLabel>
#include<QToolButton>
#include<QComboBox>
work::work(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::work)
{
    ui->setupUi(this);

    //初始化配置
    this->setWindowFlag(Qt::FramelessWindowHint);
    ui->stackedWidget->setCurrentIndex(0);

    //设置连接
    connect(ui->dButton,&QToolButton::clicked,[=](){
        ui->stackedWidget->setCurrentIndex(0);
    });
    connect(ui->mButton,&QToolButton::clicked,[=](){
        ui->stackedWidget->setCurrentIndex(1);
    });
    connect(ui->sButton,&QToolButton::clicked,[=](){
        ui->stackedWidget->setCurrentIndex(2);
    });
    connect(ui->wButton,&QToolButton::clicked,[=](){
        ui->stackedWidget->setCurrentIndex(3);
    });
    connect(ui->pushButton_4,&QPushButton::clicked,[=](){
        this->close();
    });

    //初始化数据库
    createConnection();

    //初始化控件
    ui->lineEdit->setPlaceholderText("请输入搜索内容");
    ui->lineEdit_2->setPlaceholderText("请输入搜索内容");
    ui->lineEdit_3->setPlaceholderText("请输入搜索内容");

    ui->comboBox->addItem("请选择员工学历");
    ui->comboBox_2->addItem("请选择员工状态");
    ui->comboBox_3->addItem("请选择员工部门");
    ui->comboBox_4->addItem("请选择员工部门");

    ui->comboBox->setCurrentText("请选择员工学历");
    ui->comboBox_2->setCurrentText("请选择员工状态");
    ui->comboBox_3->setCurrentText("请选择员工部门");
    ui->comboBox_4->setCurrentText("请选择员工部门");

}

//例子
void work::calculate(){
    QString a = "1";
    ui->label_20->setText(a);
}

work::~work()
{
    delete ui;
}
//点击时加载
void work::on_dButton_clicked()
{
    calculate();
}
