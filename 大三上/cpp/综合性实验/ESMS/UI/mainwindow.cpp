#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QPushButton>
#include<QLineEdit>
#include<QMessageBox>
#include<QMovie>
#include "work.h"
#include<QComboBox>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowFlag(Qt::FramelessWindowHint);

    connect(ui->exit,&QPushButton::clicked,[=](){
        this->close();
    });

    connect(ui->enter,&QPushButton::clicked,[=](){
        checkpass();
    });
    ui->acc_line->setPlaceholderText("请输入账号");
    ui->pass_line->setPlaceholderText("请输入密码");
    QMovie *movie=new QMovie(":/imagine/login1.gif");
    ui->label_4->setMovie(movie);
    movie->start();

}

//验证密码
void MainWindow::checkpass()
{
    QString acc= ui->acc_line->text();
    QString pass= ui->pass_line->text();
    if(acc=="1")//暂定用户为1就可以进入
    {
        this->close();
        w.show();
    }
    else
    {
        QMessageBox *q=new QMessageBox();
        q->setWindowFlag(Qt::FramelessWindowHint);
        q->critical(q,"错误","密码错误");
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}

