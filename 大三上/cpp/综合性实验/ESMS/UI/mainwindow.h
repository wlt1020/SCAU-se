#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "work.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    work w;
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void checkpass();
private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
