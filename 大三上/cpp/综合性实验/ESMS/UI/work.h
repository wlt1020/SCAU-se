#ifndef WORK_H
#define WORK_H

#include <QMainWindow>
#include<QPalette>
namespace Ui {
class work;
}

class work : public QMainWindow
{
    Q_OBJECT

public:

    explicit work(QWidget *parent = nullptr);
    void calculate();
    ~work();


private slots:
    void on_dButton_clicked();

private:
    Ui::work *ui;
};

#endif // WORK_H
