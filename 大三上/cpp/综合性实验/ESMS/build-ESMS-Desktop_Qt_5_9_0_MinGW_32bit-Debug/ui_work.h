/********************************************************************************
** Form generated from reading UI file 'work.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WORK_H
#define UI_WORK_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_work
{
public:
    QWidget *centralwidget;
    QWidget *widget_2;
    QLabel *label;
    QToolButton *dButton;
    QToolButton *sButton;
    QToolButton *wButton;
    QToolButton *mButton;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QWidget *widget;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_7;
    QLabel *label_18;
    QLabel *label_19;
    QLabel *label_20;
    QLabel *label_21;
    QLabel *label_22;
    QWidget *page_2;
    QWidget *widget_3;
    QHBoxLayout *horizontalLayout;
    QLabel *label_6;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QWidget *widget_5;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QComboBox *comboBox;
    QComboBox *comboBox_2;
    QTableWidget *tableWidget_3;
    QWidget *page_3;
    QTableWidget *tableWidget;
    QWidget *widget_4;
    QLabel *label_13;
    QWidget *widget_8;
    QLineEdit *lineEdit_3;
    QPushButton *pushButton_3;
    QWidget *page_4;
    QTableWidget *tableWidget_2;
    QWidget *widget_6;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_14;
    QSpacerItem *horizontalSpacer;
    QLabel *label_15;
    QLabel *label_17;
    QLabel *label_16;
    QWidget *widget_7;
    QLineEdit *lineEdit_2;
    QPushButton *pushButton_2;
    QPushButton *pushButton_4;

    void setupUi(QMainWindow *work)
    {
        if (work->objectName().isEmpty())
            work->setObjectName(QStringLiteral("work"));
        work->resize(745, 438);
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush);
        work->setPalette(palette);
        work->setStyleSheet(QStringLiteral("background-image: url(:/imagine/workback.jpg);"));
        centralwidget = new QWidget(work);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::Base, brush);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush);
        centralwidget->setPalette(palette1);
        widget_2 = new QWidget(centralwidget);
        widget_2->setObjectName(QStringLiteral("widget_2"));
        widget_2->setGeometry(QRect(10, 20, 151, 411));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::Button, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Button, brush);
        widget_2->setPalette(palette2);
        widget_2->setStyleSheet(QStringLiteral("background-image: url(:/imagine/0.jpg);"));
        label = new QLabel(widget_2);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 0, 101, 91));
        label->setStyleSheet(QStringLiteral("background-image: url(:/imagine/tx.jpg);"));
        dButton = new QToolButton(widget_2);
        dButton->setObjectName(QStringLiteral("dButton"));
        dButton->setGeometry(QRect(10, 120, 131, 51));
        QPalette palette3;
        dButton->setPalette(palette3);
        sButton = new QToolButton(widget_2);
        sButton->setObjectName(QStringLiteral("sButton"));
        sButton->setGeometry(QRect(10, 240, 131, 61));
        wButton = new QToolButton(widget_2);
        wButton->setObjectName(QStringLiteral("wButton"));
        wButton->setGeometry(QRect(10, 310, 131, 61));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::Button, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette4.setBrush(QPalette::Disabled, QPalette::Button, brush);
        wButton->setPalette(palette4);
        mButton = new QToolButton(widget_2);
        mButton->setObjectName(QStringLiteral("mButton"));
        mButton->setGeometry(QRect(10, 180, 131, 51));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::Button, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette5.setBrush(QPalette::Disabled, QPalette::Button, brush);
        mButton->setPalette(palette5);
        stackedWidget = new QStackedWidget(centralwidget);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        stackedWidget->setGeometry(QRect(180, 20, 561, 391));
        stackedWidget->setStyleSheet(QStringLiteral("background-image: url(:/imagine/0.jpg);"));
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        widget = new QWidget(page);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(40, 30, 481, 71));
        gridLayout = new QGridLayout(widget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_2 = new QLabel(widget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        label_3 = new QLabel(widget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 0, 1, 1, 1);

        label_4 = new QLabel(widget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 0, 2, 1, 1);

        label_5 = new QLabel(widget);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 0, 3, 1, 1);

        label_7 = new QLabel(widget);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 0, 4, 1, 1);

        label_18 = new QLabel(widget);
        label_18->setObjectName(QStringLiteral("label_18"));

        gridLayout->addWidget(label_18, 1, 0, 1, 1);

        label_19 = new QLabel(widget);
        label_19->setObjectName(QStringLiteral("label_19"));

        gridLayout->addWidget(label_19, 1, 1, 1, 1);

        label_20 = new QLabel(widget);
        label_20->setObjectName(QStringLiteral("label_20"));

        gridLayout->addWidget(label_20, 1, 2, 1, 1);

        label_21 = new QLabel(widget);
        label_21->setObjectName(QStringLiteral("label_21"));

        gridLayout->addWidget(label_21, 1, 3, 1, 1);

        label_22 = new QLabel(widget);
        label_22->setObjectName(QStringLiteral("label_22"));

        gridLayout->addWidget(label_22, 1, 4, 1, 1);

        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        widget_3 = new QWidget(page_2);
        widget_3->setObjectName(QStringLiteral("widget_3"));
        widget_3->setGeometry(QRect(0, 20, 541, 40));
        horizontalLayout = new QHBoxLayout(widget_3);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_6 = new QLabel(widget_3);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout->addWidget(label_6);

        label_8 = new QLabel(widget_3);
        label_8->setObjectName(QStringLiteral("label_8"));

        horizontalLayout->addWidget(label_8);

        label_9 = new QLabel(widget_3);
        label_9->setObjectName(QStringLiteral("label_9"));

        horizontalLayout->addWidget(label_9);

        label_10 = new QLabel(widget_3);
        label_10->setObjectName(QStringLiteral("label_10"));

        horizontalLayout->addWidget(label_10);

        widget_5 = new QWidget(page_2);
        widget_5->setObjectName(QStringLiteral("widget_5"));
        widget_5->setGeometry(QRect(10, 60, 531, 41));
        lineEdit = new QLineEdit(widget_5);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(10, 10, 113, 20));
        pushButton = new QPushButton(widget_5);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(130, 10, 61, 21));
        comboBox = new QComboBox(widget_5);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(210, 10, 131, 22));
        comboBox_2 = new QComboBox(widget_5);
        comboBox_2->setObjectName(QStringLiteral("comboBox_2"));
        comboBox_2->setGeometry(QRect(360, 10, 131, 22));
        tableWidget_3 = new QTableWidget(page_2);
        if (tableWidget_3->columnCount() < 9)
            tableWidget_3->setColumnCount(9);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        tableWidget_3->setObjectName(QStringLiteral("tableWidget_3"));
        tableWidget_3->setGeometry(QRect(20, 110, 530, 250));
        stackedWidget->addWidget(page_2);
        page_3 = new QWidget();
        page_3->setObjectName(QStringLiteral("page_3"));
        tableWidget = new QTableWidget(page_3);
        if (tableWidget->columnCount() < 9)
            tableWidget->setColumnCount(9);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem12);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem13);
        QTableWidgetItem *__qtablewidgetitem14 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem14);
        QTableWidgetItem *__qtablewidgetitem15 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem15);
        QTableWidgetItem *__qtablewidgetitem16 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(7, __qtablewidgetitem16);
        QTableWidgetItem *__qtablewidgetitem17 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(8, __qtablewidgetitem17);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(10, 111, 530, 251));
        widget_4 = new QWidget(page_3);
        widget_4->setObjectName(QStringLiteral("widget_4"));
        widget_4->setGeometry(QRect(30, 20, 471, 30));
        label_13 = new QLabel(widget_4);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(9, 9, 70, 16));
        widget_8 = new QWidget(page_3);
        widget_8->setObjectName(QStringLiteral("widget_8"));
        widget_8->setGeometry(QRect(30, 60, 501, 41));
        lineEdit_3 = new QLineEdit(widget_8);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(9, 10, 131, 20));
        pushButton_3 = new QPushButton(widget_8);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(150, 9, 75, 23));
        stackedWidget->addWidget(page_3);
        page_4 = new QWidget();
        page_4->setObjectName(QStringLiteral("page_4"));
        tableWidget_2 = new QTableWidget(page_4);
        if (tableWidget_2->columnCount() < 13)
            tableWidget_2->setColumnCount(13);
        QTableWidgetItem *__qtablewidgetitem18 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(0, __qtablewidgetitem18);
        QTableWidgetItem *__qtablewidgetitem19 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(1, __qtablewidgetitem19);
        QTableWidgetItem *__qtablewidgetitem20 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(2, __qtablewidgetitem20);
        QTableWidgetItem *__qtablewidgetitem21 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(3, __qtablewidgetitem21);
        QTableWidgetItem *__qtablewidgetitem22 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(4, __qtablewidgetitem22);
        QTableWidgetItem *__qtablewidgetitem23 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(5, __qtablewidgetitem23);
        QTableWidgetItem *__qtablewidgetitem24 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(6, __qtablewidgetitem24);
        QTableWidgetItem *__qtablewidgetitem25 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(7, __qtablewidgetitem25);
        QTableWidgetItem *__qtablewidgetitem26 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(8, __qtablewidgetitem26);
        QTableWidgetItem *__qtablewidgetitem27 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(9, __qtablewidgetitem27);
        QTableWidgetItem *__qtablewidgetitem28 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(10, __qtablewidgetitem28);
        QTableWidgetItem *__qtablewidgetitem29 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(11, __qtablewidgetitem29);
        QTableWidgetItem *__qtablewidgetitem30 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(12, __qtablewidgetitem30);
        tableWidget_2->setObjectName(QStringLiteral("tableWidget_2"));
        tableWidget_2->setGeometry(QRect(20, 100, 531, 271));
        widget_6 = new QWidget(page_4);
        widget_6->setObjectName(QStringLiteral("widget_6"));
        widget_6->setGeometry(QRect(30, 10, 469, 41));
        horizontalLayout_2 = new QHBoxLayout(widget_6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_14 = new QLabel(widget_6);
        label_14->setObjectName(QStringLiteral("label_14"));

        horizontalLayout_2->addWidget(label_14);

        horizontalSpacer = new QSpacerItem(286, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        label_15 = new QLabel(widget_6);
        label_15->setObjectName(QStringLiteral("label_15"));

        horizontalLayout_2->addWidget(label_15);

        label_17 = new QLabel(widget_6);
        label_17->setObjectName(QStringLiteral("label_17"));

        horizontalLayout_2->addWidget(label_17);

        label_16 = new QLabel(widget_6);
        label_16->setObjectName(QStringLiteral("label_16"));

        horizontalLayout_2->addWidget(label_16);

        widget_7 = new QWidget(page_4);
        widget_7->setObjectName(QStringLiteral("widget_7"));
        widget_7->setGeometry(QRect(30, 60, 491, 31));
        lineEdit_2 = new QLineEdit(widget_7);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(10, 7, 121, 20));
        pushButton_2 = new QPushButton(widget_7);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(140, 7, 101, 23));
        stackedWidget->addWidget(page_4);
        pushButton_4 = new QPushButton(centralwidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(670, 410, 75, 23));
        work->setCentralWidget(centralwidget);

        retranslateUi(work);

        stackedWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(work);
    } // setupUi

    void retranslateUi(QMainWindow *work)
    {
        work->setWindowTitle(QApplication::translate("work", "MainWindow", Q_NULLPTR));
        label->setText(QString());
        dButton->setText(QApplication::translate("work", "\350\277\220\350\220\245\346\225\260\346\215\256", Q_NULLPTR));
        sButton->setText(QApplication::translate("work", "\345\207\272\345\213\244\347\273\237\350\256\241", Q_NULLPTR));
        wButton->setText(QApplication::translate("work", "\345\267\245\350\265\204\346\212\245\350\241\250", Q_NULLPTR));
        mButton->setText(QApplication::translate("work", "\345\221\230\345\267\245\347\256\241\347\220\206", Q_NULLPTR));
        label_2->setText(QApplication::translate("work", "\350\277\237\345\210\260\344\272\272\346\225\260", Q_NULLPTR));
        label_3->setText(QApplication::translate("work", "\350\257\267\345\201\207\344\272\272\346\225\260", Q_NULLPTR));
        label_4->setText(QApplication::translate("work", "\346\227\267\345\267\245\344\272\272\346\225\260", Q_NULLPTR));
        label_5->setText(QApplication::translate("work", "\346\227\251\351\200\200\344\272\272\346\225\260", Q_NULLPTR));
        label_7->setText(QApplication::translate("work", "\346\234\252\346\211\223\345\215\241\344\272\272\346\225\260", Q_NULLPTR));
        label_18->setText(QApplication::translate("work", "TextLabel", Q_NULLPTR));
        label_19->setText(QApplication::translate("work", "TextLabel", Q_NULLPTR));
        label_20->setText(QApplication::translate("work", "TextLabel", Q_NULLPTR));
        label_21->setText(QApplication::translate("work", "TextLabel", Q_NULLPTR));
        label_22->setText(QApplication::translate("work", "TextLabel", Q_NULLPTR));
        label_6->setText(QApplication::translate("work", "\345\221\230\345\267\245\347\256\241\347\220\206                                                        ", Q_NULLPTR));
        label_8->setText(QApplication::translate("work", "\345\221\230\345\267\245", Q_NULLPTR));
        label_9->setText(QApplication::translate("work", "TextLabel", Q_NULLPTR));
        label_10->setText(QApplication::translate("work", "\344\272\272", Q_NULLPTR));
        pushButton->setText(QApplication::translate("work", "\346\220\234\347\264\242", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem = tableWidget_3->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("work", "\345\267\245\345\217\267", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget_3->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("work", "\345\247\223\345\220\215", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget_3->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("work", "\346\211\213\346\234\272", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget_3->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("work", "\345\255\246\344\275\215", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget_3->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("work", "\347\212\266\346\200\201", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget_3->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QApplication::translate("work", "\351\203\250\351\227\250", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget_3->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QApplication::translate("work", "\351\202\256\347\256\261", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget_3->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QApplication::translate("work", "\346\230\257\345\220\246\350\275\254\346\255\243", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidget_3->horizontalHeaderItem(8);
        ___qtablewidgetitem8->setText(QApplication::translate("work", "\345\205\245\350\201\214\346\227\245\346\234\237", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem9 = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem9->setText(QApplication::translate("work", "\345\207\272\345\213\244ID", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem10 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem10->setText(QApplication::translate("work", "\345\221\230\345\267\245\345\267\245\345\217\267", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem11 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem11->setText(QApplication::translate("work", "\346\227\251\351\200\200\346\254\241\346\225\260", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem12 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem12->setText(QApplication::translate("work", "\350\277\237\345\210\260\346\254\241\346\225\260", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem13 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem13->setText(QApplication::translate("work", "\346\234\252\346\211\223\345\215\241\346\254\241\346\225\260", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem14 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem14->setText(QApplication::translate("work", "\345\212\240\347\217\255\346\254\241\346\225\260", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem15 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem15->setText(QApplication::translate("work", "\350\257\267\345\201\207\346\254\241\346\225\260", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem16 = tableWidget->horizontalHeaderItem(7);
        ___qtablewidgetitem16->setText(QApplication::translate("work", "\345\272\224\345\207\272\345\213\244\345\244\251\346\225\260", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem17 = tableWidget->horizontalHeaderItem(8);
        ___qtablewidgetitem17->setText(QApplication::translate("work", "\350\200\203\345\213\244\346\227\245\346\234\237", Q_NULLPTR));
        label_13->setText(QApplication::translate("work", "\345\207\272\345\213\244\346\203\205\345\206\265", Q_NULLPTR));
        pushButton_3->setText(QApplication::translate("work", "\346\220\234\347\264\242", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem18 = tableWidget_2->horizontalHeaderItem(0);
        ___qtablewidgetitem18->setText(QApplication::translate("work", "\345\267\245\345\217\267", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem19 = tableWidget_2->horizontalHeaderItem(1);
        ___qtablewidgetitem19->setText(QApplication::translate("work", "\345\237\272\346\234\254\345\267\245\350\265\204", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem20 = tableWidget_2->horizontalHeaderItem(2);
        ___qtablewidgetitem20->setText(QApplication::translate("work", "\345\262\227\344\275\215\345\267\245\350\265\204", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem21 = tableWidget_2->horizontalHeaderItem(3);
        ___qtablewidgetitem21->setText(QApplication::translate("work", "\347\273\251\346\225\210\345\267\245\350\265\204", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem22 = tableWidget_2->horizontalHeaderItem(4);
        ___qtablewidgetitem22->setText(QApplication::translate("work", "\347\244\276\344\277\235\346\211\243\346\254\276", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem23 = tableWidget_2->horizontalHeaderItem(5);
        ___qtablewidgetitem23->setText(QApplication::translate("work", "\345\205\250\345\213\244\345\245\226", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem24 = tableWidget_2->horizontalHeaderItem(6);
        ___qtablewidgetitem24->setText(QApplication::translate("work", "\346\227\267\345\267\245\346\211\243\350\264\271", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem25 = tableWidget_2->horizontalHeaderItem(7);
        ___qtablewidgetitem25->setText(QApplication::translate("work", "\350\277\237\345\210\260\346\211\243\350\264\271", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem26 = tableWidget_2->horizontalHeaderItem(8);
        ___qtablewidgetitem26->setText(QApplication::translate("work", "\346\227\251\351\200\200\346\211\243\350\264\271", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem27 = tableWidget_2->horizontalHeaderItem(9);
        ___qtablewidgetitem27->setText(QApplication::translate("work", "\345\212\240\347\217\255\350\264\271", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem28 = tableWidget_2->horizontalHeaderItem(10);
        ___qtablewidgetitem28->setText(QApplication::translate("work", "\345\272\224\345\217\221\345\267\245\350\265\204", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem29 = tableWidget_2->horizontalHeaderItem(11);
        ___qtablewidgetitem29->setText(QApplication::translate("work", "\345\256\236\345\217\221\345\267\245\350\265\204", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem30 = tableWidget_2->horizontalHeaderItem(12);
        ___qtablewidgetitem30->setText(QApplication::translate("work", "\345\267\245\350\265\204\346\227\245\346\234\237", Q_NULLPTR));
        label_14->setText(QApplication::translate("work", "\345\267\245\350\265\204\346\212\245\350\241\250", Q_NULLPTR));
        label_15->setText(QApplication::translate("work", "\345\221\230\345\267\245", Q_NULLPTR));
        label_17->setText(QApplication::translate("work", "TextLabel", Q_NULLPTR));
        label_16->setText(QApplication::translate("work", "\344\272\272", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("work", "\346\220\234\347\264\242", Q_NULLPTR));
        pushButton_4->setText(QApplication::translate("work", "\351\200\200\345\207\272", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class work: public Ui_work {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WORK_H
