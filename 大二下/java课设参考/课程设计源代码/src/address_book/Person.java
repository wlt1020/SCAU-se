/*package address_book;

import javafx.beans.property.SimpleStringProperty;

public class Person {
	SimpleStringProperty name; // 姓名
	SimpleStringProperty telephone; // 电话
	SimpleStringProperty phone; // 手机
	SimpleStringProperty email; // 电子邮箱
	SimpleStringProperty birthday; // 生日
	SimpleStringProperty workplace; // 工作地
	SimpleStringProperty homeaddres; // 家庭地址
	SimpleStringProperty postcode; // 邮编
	SimpleStringProperty group[]; // 所属组，至少要有一个
	SimpleStringProperty note; // 备注
	//SimpleStringProperty photopath; // 照片的地址
	// SimpleStringProperty filename;

	public Person(String name, String telephone, String phone, String email, String birthday, String workplace,
			String homeaddres, String postcode, String[] group, String note) {
		this.name = new SimpleStringProperty(name);
		this.telephone = new SimpleStringProperty(telephone);
		this.phone = new SimpleStringProperty(phone);
		this.email = new SimpleStringProperty(email);
		this.birthday = new SimpleStringProperty(birthday);
		this.workplace = new SimpleStringProperty(workplace);
		this.homeaddres = new SimpleStringProperty(homeaddres);
		this.postcode = new SimpleStringProperty(postcode);
		this.group = new SimpleStringProperty(group);

		for (int i = 0; i < Group.length; i++)
			this.group[i] = new SimpleStringProperty(Group[i]);

		this.note = new SimpleStringProperty(note);
		//this.photopath = new SimpleStringProperty(photopath);

	}

	public void setName(String Name) {
		name.set(Name);
	}

	public String getName() {
		return name.get();
	}

	public void setTelephone(String Telephone) {
		telephone.set(Telephone);
	}

	public String getTelephone() {
		return telephone.get();
	}

	public void setPhone(String Phone) {
		phone.set(Phone);
	}

	public String getPhone() {
		return phone.get();
	}

	public void setEmail(String Email) {
		email.set(Email);
		;
	}

	public String getEmail() {
		return email.get();
	}

	public void setBirthday(String Birthday) {
		email.set(Birthday);
		;
	}

	public String getBirthday() {
		return birthday.get();
	}

	public void setWorkplace(String Workplace) {
		workplace.set(Workplace);
		;
	}

	public String getWorkplace() {
		return workplace.get();
	}

	public void setHomeaddres(String Homeaddres) {
		homeaddres.set(Homeaddres);
	}

	public String getHomeaddres() {
		return homeaddres.get();
	}

	public void setPostcode(String Postcode) {
		postcode.set(Postcode);
	}

	public String getPostcode() {
		return postcode.get();
	}
	public void setGroup(String[] Group) {
		for(int i=0; i<Group.length; i++)
		group[i].set(Group[i]);
	}

	public String[] getGroup() {
		String group1[] = null;
		for(int i=0; i<group.length; i++)
		group1[i] = group[i].get();
		return group1;
	}
	
	public void setNote(String Note) {
		note.set(Note);
	}

	public String getNote() {
		return note.get();
	}
	
	public void setPhotopath(String Photopath) {
		photopath.set(Photopath);
	}

	public String getPhotopath() {
		return photopath.get(); 
	}
}*/

package address_book;

//姓名、电话、手机、电子邮箱、
//生日、相片、工作单位 、家庭地址、邮编、所属组、备注 
//所属组不能超过10000个

public class Person {
	private String name = new String();  //姓名
	private String telephone = new String();  //电话
	private String phone= new String();  //手机
	private String email = new String();  //电子邮箱
	private String birthday = new String();  //生日 
	private String workplace = new String();  //工作地
	private String homeaddres = new String();  //家庭地址
	private String postcode = new String();  //邮编
	private String group = new String();  //所属组，至少要有一个
	private String note = new String();  //备注
	private String photopath = new String();  //照片的地址
	
	public Person()
	{
		
	}
	public String getPhotopath() {
		return photopath;
	}
	public void setPhotopath(String photopath) {
		this.photopath = photopath;
	}
	public Person(String name,String telephone,String phone,String email,String birthday,
			String workplace,String homeaddres,String postcode,String group,String note,String photopath) {
		this.name = name;
		this.telephone = telephone;
		this.phone = phone;
		this.email = email;
		this.birthday = birthday;
		this.workplace = workplace;
		this.homeaddres = homeaddres;
		this.postcode = postcode;
		this.group = group;
		this.note = note;
		this.photopath = photopath;
	}
		
	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getWorkplace() {
		return workplace;
	}

	public void setWorkplace(String workplace) {
		this.workplace = workplace;
	}

	public String getHomeaddres() {
		return homeaddres;
	}

	public void setHomeaddres(String homeaddres) {
		this.homeaddres = homeaddres;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
}
