package address_book;

import java.io.IOException;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color; 
import javafx.scene.paint.Paint;


public class Main extends Application{  
	
    @Override
    public void start(Stage primaryStage) throws IOException {  
        BorderPane primaryBorderPane=new BorderPane();//最外面板   
        primaryBorderPane.setStyle("-fx-border-color:lightblue;-fx-background-:lightblue");
        BorderPane borderPane=new BorderPane();//文件面板 
        borderPane.setStyle("-fx-border-color:lightblue;-fx-background-:lightRed");

        FileMenu filemenu=new FileMenu();//定义各组件实例
        borderPane.setCenter(filemenu.Sum());
        primaryBorderPane.setTop(borderPane);

        primaryBorderPane.setCenter(filemenu.getInTable());//添加通讯录信息模块
       // primaryBorderPane.setRight(filemenu.getHomePage());
        primaryBorderPane.setLeft(filemenu.getContackGroups());
        primaryBorderPane.setBottom(filemenu.getTextPath()); 
       
		primaryStage.getIcons().add(new Image("/image/address.jpg"));
        primaryStage.setScene(new Scene(primaryBorderPane,1200,600));
        primaryStage.setTitle("通讯录管理系统");
        primaryStage.show();
    }
    public static void main(String[] args) {
        // TODO code application logic here
        launch(args);
    }
}