package address_book;

import javax.swing.JButton;
import java.io.BufferedReader;
import java.io.EOFException;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ListView;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import jdk.nashorn.internal.ir.Flags;
import net.sourceforge.cardme.io.CompatibilityMode;
import net.sourceforge.cardme.io.VCardWriter;
import net.sourceforge.cardme.vcard.VCardImpl;
import net.sourceforge.cardme.vcard.arch.ParameterTypeStyle;
import net.sourceforge.cardme.vcard.arch.VCardVersion;
import net.sourceforge.cardme.vcard.types.AdrType;
import net.sourceforge.cardme.vcard.types.BDayType;
import net.sourceforge.cardme.vcard.types.EmailType;
import net.sourceforge.cardme.vcard.types.ExtendedType;
import net.sourceforge.cardme.vcard.types.FNType;
import net.sourceforge.cardme.vcard.types.NoteType;
import net.sourceforge.cardme.vcard.types.TelType;
import net.sourceforge.cardme.vcard.types.params.AdrParamType;
import net.sourceforge.cardme.vcard.types.params.TelParamType;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.sun.glass.ui.Application;
import com.sun.xml.internal.ws.api.ha.StickyFeature;

import ezvcard.io.text.VCardReader;
import ezvcard.property.Address;
import ezvcard.property.Birthday;
import ezvcard.property.Email;
import ezvcard.property.FormattedName;
import ezvcard.property.Telephone;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class FileMenu {
	private final MenuButton menu1 = new MenuButton("导入");
	private final MenuButton menu2 = new MenuButton("导出");
	
	private final MenuItem CsvRead = new MenuItem("打开Csv文件通讯录"), VcardRead = new MenuItem("打开Vcard文件通讯录"),
			CsvWrite = new MenuItem("保存为Csv文件通讯录"), VcardWrite = new MenuItem("保存为Vcard文件通讯录");
	Button b1 = new Button("添加"); // 添加联系人
	Button b2 = new Button("删除"); // 删除联系人
	Button b3 = new Button("编辑"); // 编辑联系人
	Button b5 = new Button("详情"); //查看联系人
	Button b4 = new Button("刷新"); //刷新数据
	private final InTable inTable = new InTable();// 定义通讯录实例
	private final HomePage homepage = new HomePage();// 定义主页实例
	private final TextField tfCheck = new TextField();// 输入查询文本框
	private final AddPerson addperson = new AddPerson(); // 定义添加联系人实例
	private final ModifyPerson modifyperson = new ModifyPerson();  //定义修改联系人实例
	private final ShowPerson showPerson = new ShowPerson();  //定义展示联系人详情
	private final Text textPath = new Text();
	private final String path = new String();// 文件路径
	private final ContackGroups contackgroups = new ContackGroups(inTable); // 定义联系组实例

	FileMenu() {

	}
	
	public InTable getInTable() {
		return inTable;
	}

	/**
	 *  返回文件路径
	 * @return
	 */
	public Text getTextPath() {
		textPath.setVisible(true);
		return textPath;
	}

	public HomePage getHomePage() {
		return homepage;
	}

	/**
	 *  联系组
	 * @return
	 */
	public VBox getContackGroups() {
		VBox vbox1 = new VBox();
		vbox1.getChildren().addAll(contackgroups.AddGroup(), contackgroups.choose(inTable),contackgroups.check());
		HBox hBox2 = new HBox();
		hBox2.getChildren().add(contackgroups.addField());
		hBox2.setPadding(new Insets(20, 30, 30, 20)); // 节点到边缘的距离
		VBox vbox2 = new VBox();
		vbox2.getChildren().addAll(contackgroups.showContackGroups(inTable), vbox1,hBox2);
		return vbox2;
	}

	public HBox Sum() {
		HBox hbox1 = new HBox();
		HBox hbox2 = new HBox();
		menu1.getItems().addAll(CsvRead, VcardRead);

		// 打开csv通讯录
		CsvRead.setOnAction(e -> {
			csvread();
		});
		// 打开vcard通讯录
		VcardRead.setOnAction(e -> {
			vcardread();
		});

		menu2.getItems().addAll(CsvWrite, VcardWrite);

		// 保存csv通讯录
		CsvWrite.setOnAction(e -> {
			csvwrite();

		});
		// 保存vcard通讯录
		VcardWrite.setOnAction(e -> {
			vcardwrite();
		});
		
		/*Text text = new Text("搜索:");
		text.setFont(Font.font(15));*/
		Button sousuo = new Button("搜索");
		sousuo.setPrefSize(60, 20);
		sousuo.setStyle(" -fx-border-color: lightblue;-fx-background-color: lightblue");
		tfCheck.setPromptText("请输入内容按回车键查询");
		tfCheck.setOnAction(e -> {
			inTable.updateFilterInformation(tfCheck.getText());
		});

		hbox2.getChildren().addAll(tfCheck,sousuo);
		hbox2.setSpacing(2);

		b1.setPrefSize(60, 20);
		b2.setPrefSize(60, 20);  
		b3.setPrefSize(60, 20);
		b4.setPrefSize(60, 20);
		b5.setPrefSize(60, 20);
		b1.setStyle(" -fx-border-color: lightblue;-fx-background-color: lightblue");
		b2.setStyle(" -fx-border-color: yellow;-fx-background-color: yellow");
		b3.setStyle(" -fx-border-color: lightgreen;-fx-background-color: lightgreen");
		b4.setStyle(" -fx-border-color: lightblue;-fx-background-color: lightblue");
		b5.setStyle(" -fx-border-color: lightgreen;-fx-background-color: lightgreen");

		/**
		 * 增加联系人
		 */
		b1.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				try {
					addperson.showWindow();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		/**
		 * 删除联系人
		 */
		b2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				int index = inTable.getSelectionModel().getSelectedIndex();
				if (index<0) {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Warning");
					alert.setHeaderText("尚未选择删除项");
					alert.showAndWait();
				}else {
					inTable.getItems().remove(index);
				}
			}
		});
		/**
		 * 修改联系人
		 */
		b3.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				try {
					int index = inTable.getSelectionModel().getSelectedIndex();
					//把选中元素传过去
					if (index<0) {
						Alert alert = new Alert(AlertType.WARNING);
						alert.setTitle("Warning");
						alert.setHeaderText("尚未选择编辑项");
						alert.showAndWait();
					}else {
						modifyperson.showWindow(inTable.getObservableList().get(index),index);
						
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
		/**
		 * 刷新
		 */
		b4.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				//新增刷新
				if (addperson.getInformation()!=null) {
					inTable.addInformation(addperson.getInformation());
					addperson.setInformation(null);
				}
				//修改刷新
				if(modifyperson.getInformation()!=null) {
					ObservableList<Person> olOriginalInformation = inTable.getObservableList();
					olOriginalInformation.set(modifyperson.getIndex(), modifyperson.getPerson());
					inTable.setOlOriginalInformation(olOriginalInformation);
					//inTable.getItems().set(modifyperson.getIndex(), modifyperson.getPerson());
					modifyperson.setInformation(null);
				}
				//联系组删除刷新
				if (contackgroups.getInformation()!=null) {
					ObservableList<Person> olOriginalInformation = inTable.getObservableList();
					for (int i=0;i<olOriginalInformation.size();i++) {
						olOriginalInformation.set(i, contackgroups.getPt().get(i));
					}
					inTable.setOlOriginalInformation(olOriginalInformation);
					contackgroups.setInformation(null);
				}
				//联系组查询
				if (contackgroups.getGrname()!=null) {
					inTable.updateFilterInformation(contackgroups.getGrname());
					contackgroups.setGrname(null);
				}
			}
		});
		/**
		 * 查看联系人详情
		 */
		b5.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				try {
					int index = inTable.getSelectionModel().getSelectedIndex();
					if (index<0) {
						Alert alert = new Alert(AlertType.WARNING);
						alert.setTitle("Warning");
						alert.setHeaderText("尚未选择联系人");
						alert.showAndWait();
					}//把选中元素传过去
					else {
						showPerson.showWindow(inTable.getObservableList().get(index));
					}
					
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
		hbox1.getChildren().addAll(menu1, menu2, b1, b2, b3,b4,b5);
		hbox1.setSpacing(15);// 设间距

		HBox hbox3 = new HBox();
		// hbox.setAlignment(Pos.CENTER);
		hbox3.getChildren().addAll(hbox1,hbox2);
		hbox3.setSpacing(50);// 设间距
		return hbox3;
	}

	// 保存Csv文件
	public void csvwrite() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(// 添加过滤器
				new ExtensionFilter("csv文件(*.csv)", "*.csv"), new ExtensionFilter("所有文件", "*.*"));
		fileChooser.setInitialFileName("tongxunlu.csv");// 设置默认文件名
		File file = fileChooser.showSaveDialog(null);
		if (file != null) {

			try (PrintWriter output = new PrintWriter(file, "GBK")) {// 用GBK写

				for (Person data : inTable.getObservableList()) {
					String str = data.getName() + "," + data.getTelephone() + "," + data.getPhone() + ","
							+ data.getEmail() + "," + data.getBirthday() + "," + data.getWorkplace() + ","
							+ data.getHomeaddres() + "," + data.getPostcode() + "," + data.getGroup() + ","
							+ data.getNote()+","+data.getPhotopath();
					output.println(str);
				}
//                    output.close();//output要关闭，否则数据写入不成功,可改用try(PrintWriter output=new PrintWriter()){ ....}
				System.out.println("保存成功");
			} catch (FileNotFoundException ex) {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Warning");
				alert.setHeaderText("文件打开错误");
				alert.showAndWait();
				ex.printStackTrace();
			} catch (UnsupportedEncodingException ex) {
				Logger.getLogger(FileMenu.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	// 导入Csv文件
	public void csvread() {
		FileChooser fileChooser = new FileChooser();// 选择文件控件

		fileChooser.getExtensionFilters().addAll(// 添加过滤器
				new ExtensionFilter("csv文件(*.csv)", "*.csv"), new ExtensionFilter("所有文件", "*.*"));

		File file = fileChooser.showOpenDialog(null);

		if (file != null && file.getName().endsWith(".csv")) {
			try {
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), "GBK");// 考虑utf-8编码
				BufferedReader bufferedReader = new BufferedReader(read);
				String str = new String();
				// 读一行
				while ((str = bufferedReader.readLine()) != null) {
					if (str.trim().length() > 0) {// 排除空行
						inTable.addInformation(str);
					}
				}
				read.close();
				contackgroups.showContackGroups(inTable);
			} catch (FileNotFoundException ex) {
				System.out.println("File is not exit");
				ex.printStackTrace();
			} catch (IOException ex) {
				Logger.getLogger(FileMenu.class.getName()).log(Level.SEVERE, null, ex);
			}
		} else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning");
			alert.setHeaderText("文件打开错误");
			alert.showAndWait();
		}
	}

	/**
	 * 导出Vcard文件
	 */
	public void vcardwrite() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(// 添加过滤器
				new ExtensionFilter("vcard文件(*.vcf)", "*.vcf"), new ExtensionFilter("所有文件", "*.*"));
		fileChooser.setInitialFileName("tongxunlu.vcf");// 设置默认文件名
		File file = fileChooser.showSaveDialog(null);
		if (file != null) {

			try {
				VCardWriter writer = new VCardWriter(VCardVersion.V3_0, CompatibilityMode.RFC2426);// 用户把VCard转换为字符
				FileWriter fw = new FileWriter(new File(file.getAbsolutePath()));// 把VCard数据（字符）写入文件
				String[] str = new String[inTable.getObservableList().size() + 1];
				int i = 0;
				for (Person data : inTable.getObservableList()) {
					VCardImpl vc = new VCardImpl();// 创建一个名片
					//开始写入
					//名字
					vc.setFN(new FNType(data.getName()));
					// 家庭电话
					TelType hometel = new TelType();
					hometel.setCharset("UTF-8");
					hometel.setTelephone(data.getTelephone());
					hometel.addParam(TelParamType.HOME);
					hometel.setParameterTypeStyle(ParameterTypeStyle.PARAMETER_VALUE_LIST);
					vc.addTel(hometel);
					// 手机
					TelType phone = new TelType();
					phone.setCharset("UTF-8");
					phone.setTelephone(data.getPhone());
					phone.addParam(TelParamType.CELL);
					phone.setParameterTypeStyle(ParameterTypeStyle.PARAMETER_VALUE_LIST);
					vc.addTel(phone);
					// 电子邮箱
					vc.addEmail(new EmailType(data.getEmail()));
					// 生日
					Calendar birthday = Calendar.getInstance();
					birthday.clear();
					birthday.set(Calendar.YEAR, 2000);
					birthday.set(Calendar.MONTH, 10 - 1);
					birthday.set(Calendar.DATE, 1 + 1);
					vc.setBDay(new BDayType(birthday));
					// 照片
					ExtendedType xphotopath = new ExtendedType("X-Photopath", data.getPhotopath());
					vc.addExtendedType(xphotopath);

					// 工作单位
					AdrType address2 = new AdrType();
					address2.setCharset("UTF-8");
					address2.setExtendedAddress("");
					address2.setStreetAddress(data.getWorkplace());
					address2.addParam(AdrParamType.WORK);
					vc.addAdr(address2);
					// 家庭住址
					AdrType address1 = new AdrType();
					address1.setCharset("UTF-8");
					address1.setExtendedAddress("");
					address1.setStreetAddress(data.getHomeaddres());
					address1.setPostalCode(data.getPostcode());// 邮编
					address1.addParam(AdrParamType.HOME);
					vc.addAdr(address1);

					// 所属组
					String group = data.getGroup();
					ExtendedType xGroup = new ExtendedType("X-Group", group);
					vc.addExtendedType(xGroup);

					// 备注
					NoteType note = new NoteType();
					note.setNote(data.getNote());
					vc.addNote(note);
					
					
					writer.setVCard(vc);
					str[i] = writer.buildVCardString();// 把名片对象转化为字符
					i++;
				}
				
				String vcString = new String();
				for (int j = 0; j < str.length - 1; j++) {
					vcString = vcString + str[j];
				}
				fw.append(vcString);// 写入文件
				fw.flush();
				fw.close();
				System.out.println("保存成功");
			} catch (Exception e) {
				// TODO: handle exception
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Warning");
				alert.setHeaderText("文件打开错误");
				alert.showAndWait();
			}
		}
	}

    /**
     * 导入Vcard文件
     */
	public void vcardread() {
		FileChooser fileChooser = new FileChooser();// 选择文件控件
		fileChooser.getExtensionFilters().addAll(// 添加过滤器
				new ExtensionFilter("vcard文件(*.vcf)", "*.vcf"), new ExtensionFilter("所有文件", "*.*"));
		File file = fileChooser.showOpenDialog(null);

		if (file != null && file.getName().endsWith(".vcf")) {
			try {
				VCardReader reader = new VCardReader(file);
				ezvcard.VCard vcard;
				while ((vcard = reader.readNext()) != null) {
					String pttemp = new String();
					// 姓名
					FormattedName fn = vcard.getFormattedName();
					pttemp = pttemp + fn.getValue() + ",";
					// 家庭电话
					Telephone tel = vcard.getTelephoneNumbers().get(0);
					pttemp = pttemp + tel.getText() + ",";
					// 手机
					Telephone phone = vcard.getTelephoneNumbers().get(1);
					pttemp = pttemp + (phone.getText()) + ",";
					// 电子邮箱
					Email email = vcard.getEmails().get(0);
					pttemp = pttemp + (email.getValue()) + ",";
					// 生日
					DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
					Birthday bday = vcard.getBirthday();
					Date date = bday.getDate();
					date.setDate(date.getDate() - 1);
					String birthday = df.format(date);
					pttemp = pttemp + (birthday) + ",";
					// 工作地址
					Address addr = vcard.getAddresses().get(0);
					pttemp = pttemp + (addr.getStreetAddress()) + ",";
					// 家庭地址
					Address homeaddr = vcard.getAddresses().get(1);
					pttemp = pttemp + (homeaddr.getStreetAddress()) + ",";
					// 邮编
					pttemp = pttemp + (homeaddr.getPostalCode()) + ",";
					// 所属组
					String group = vcard.getExtendedProperties().get(1).getValue();
					pttemp = pttemp + (group) + ",";
					// 备注
					pttemp = pttemp + (vcard.getNotes().get(0).getValue()) + ",";
					// 图片位置
					pttemp = pttemp + (vcard.getExtendedProperties().get(0).getValue()) ;
					
					inTable.addInformation(pttemp);// 数据放入表内
					contackgroups.showContackGroups(inTable);
				}
				System.out.println("导入成功");
			} catch (Exception e) {
				// TODO: handle exception
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Warning");
				alert.setHeaderText("文件打开错误");
				alert.showAndWait();
			}
		} else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning");
			alert.setHeaderText("文件打开错误");
			alert.showAndWait();
		}
		
	}
}