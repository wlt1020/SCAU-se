package address_book;

import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import address_book.Person;
public class InTable extends TableView {

    private TableColumn namecol = new TableColumn("姓名");
    private TableColumn telephonecol = new TableColumn("电话");
    private TableColumn	phonecol = new TableColumn("手机"); 
    private TableColumn emailcol = new TableColumn("电子邮箱");
    private TableColumn	birthdaycol = new TableColumn("生日");
    private TableColumn workplacecol = new TableColumn("工作地");
    private TableColumn homeaddrescol = new TableColumn("家庭住址");
    private TableColumn	postcodecol = new TableColumn("邮编"); 
    private TableColumn groupcol = new TableColumn("组别");
    private TableColumn notecol = new TableColumn("备注");
    private ObservableList<Person> olOriginalInformation = FXCollections.observableArrayList(),//为实现查询，保存原始数据
            olFilterInformation = FXCollections.observableArrayList();//过滤后的数据，注：由于需要用到动态的ObservableList,不要用static修饰 

	public InTable() {
        this(",".split("[,]"));
    }
    /**
     * 标题栏
     * @param Information
     */
    public InTable(String[] Information) {
        getColumns().addAll(namecol, telephonecol, phonecol,emailcol,birthdaycol
        		,workplacecol,homeaddrescol,postcodecol,groupcol,notecol);
        
        namecol.setCellValueFactory(new PropertyValueFactory<Person, String>("name")); 
        telephonecol.setCellValueFactory(new PropertyValueFactory<Person, String>("telephone"));
        phonecol.setCellValueFactory(new PropertyValueFactory<Person, String>("phone"));
        emailcol.setCellValueFactory(new PropertyValueFactory<Person, String>("email"));
        birthdaycol.setCellValueFactory(new PropertyValueFactory<Person, String>("birthday"));
        workplacecol.setCellValueFactory(new PropertyValueFactory<Person, String>("workplace")); 
        homeaddrescol.setCellValueFactory(new PropertyValueFactory<Person, String>("homeaddres"));
        postcodecol.setCellValueFactory(new PropertyValueFactory<Person, String>("postcode"));
        groupcol.setCellValueFactory(new PropertyValueFactory<Person, String>("group"));
        notecol.setCellValueFactory(new PropertyValueFactory<Person, String>("note"));
        namecol.setMinWidth(50);
        telephonecol.setMinWidth(50);
        phonecol.setMinWidth(70);
        emailcol.setMinWidth(80);
        birthdaycol.setMinWidth(50);
        workplacecol.setMinWidth(50);
        homeaddrescol.setMinWidth(50);
        postcodecol.setMinWidth(50);
        groupcol.setMinWidth(80);
        notecol.setMinWidth(50);
        addInformation(Information);
        setItems(olFilterInformation);//向表格添加数据
    }
    /**
     * 加入数据组（导入）
     * @param strInformation
     */
    public void addInformation(String[] strInformation ) {
        for (int i=0; i<strInformation.length;i++) {
            addInformation(strInformation[i]);
        }
    }
    /**
     * 加入单个数据
     * @param strInformation
     */
    public void addInformation(String strInformation) {
        if(strInformation.trim().length()<1)//若为空
            return ;
        String[] strPerson = strInformation.trim().split("[,]");
        olFilterInformation.add(new Person(strPerson[0], strPerson[1], strPerson[2], strPerson[3], strPerson[4], strPerson[5], strPerson[6], strPerson[7],strPerson[8],strPerson[9],strPerson[10]));
        olOriginalInformation.add(new Person(strPerson[0], strPerson[1], strPerson[2], strPerson[3], strPerson[4], strPerson[5], strPerson[6], strPerson[7], strPerson[8],strPerson[9],strPerson[10]));
    }
    /**
     * 获取单个数据
     * @param index
     * @return
     */
    public Person getInformation(int index) {
		return olOriginalInformation.get(index);
	}
    /**
     * 获取长度
     * @return
     */
    public int getInformationLenth() {
		return olOriginalInformation.size();
	}
  /**
   * 清空数据
   */
    public void cleanData() {
        olFilterInformation.clear();
        olOriginalInformation.clear();
    }
    
    
    /**
     * 是否为字母
     * @param tap
     * @return
     */
    public static boolean isWord(String tap) {
		{
			Pattern pattern = Pattern.compile("([a-z]|[A-Z])*");
			return pattern.matcher(tap).matches();
		}
	}

	/**
	 * 判断是否为数字
	 * 
	 * @param str
	 * @return true 表示是数字
	 */
	public static boolean isNumber(String str) {
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isDigit(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 判断字符串中是否包含中文
	 * 
	 * @param str 待校验字符串
	 * @return 是否为中文
	 * @warn 不能校验是否为中文标点符号
	 */
	public static boolean isContainChinese(String str) {
		Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
		Matcher m = p.matcher(str);
		if (m.find()) {
			return true;
		}
		return false;
	}
	
	
	/**
	 * 模糊查找 姓名、电话、手机、
	 * 
	 * @param text
	 * @return ptshow 返回一个PeopleTable的list
	 * 
	 */
	public void updateFilterInformation(String text) {
		 olFilterInformation.clear();
	    if (text.trim().length() < 1) {//如果保留字段为空
	            olFilterInformation.addAll(olOriginalInformation);
	            System.out.println("字段为空");
	        } 
	    else if (isContainChinese(text)) {
			// 有中文
			for (Person data :olOriginalInformation ) {
				if (data.getName().indexOf(text) != -1) {
                    olFilterInformation.add(data);
				}
				else if (data.getGroup().indexOf(text)!=-1) {
					olFilterInformation.add(data);
				}
			}
		}
		// 全为数字，直接查电话就好了
		else if (isNumber(text)) {
			for (Person data : olOriginalInformation) {
				if (data.getPhone().indexOf(text) != -1 || data.getTelephone().indexOf(text) != -1) {
					// 有这个数字在手机(电话)中
                    olFilterInformation.add(data);
				}
			}
		}
		// 全为字母
		else if (isWord(text)) {
			for (Person data : olOriginalInformation) {
				String name = data.getName();// 名字
				String pinyinname = PinYin.getPinYin(name);// 拼音名字
				String capitalname = PinYin.getPinYinHeadCharLower(name);// 名字首字母  
				if (pinyinname.indexOf(text) != -1 || capitalname.indexOf(text) != -1) {
					// 名字里有这个字
                    olFilterInformation.add(data);
				}
			}
		}
	}
     //返回过滤后的数据
    public ObservableList<Person> getObservableList() {
        return olFilterInformation;
    }
    
    public ObservableList<Person> getOlOriginalInformation() {
		return olOriginalInformation;
	}
    
    public void setOlOriginalInformation(ObservableList<Person> olOriginalInformation) {
		this.olOriginalInformation = olOriginalInformation;
	}
}
