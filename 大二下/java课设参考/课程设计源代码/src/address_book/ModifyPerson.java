package address_book;

import java.io.File;
import java.io.FileInputStream;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.ImageViewBuilder;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;

public class ModifyPerson extends Application{
	static Stage stage=new Stage();
	private String information = null;
	private int index = -1;
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	private Person person = new Person();
	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}
	public void start(Stage primaryStage) throws Exception{
		Label l_name = new Label("姓名:");
		Label l_telephone = new Label("电话:");
		Label l_phone = new Label("手机:");
		Label l_email = new Label("电子邮箱:");
		Label l_birthday = new Label("生日:");
		Label l_workplace = new Label("工作地:");
		Label l_homeaddress = new Label("家庭住址:");
		Label l_postcode = new Label("邮编:");
		Label l_group = new Label("组别:");
		Label l_note = new Label("备注:");
		
		TextField t_name = new TextField();//名字
		t_name.setText(person.getName());
		TextField t_telephone = new TextField();
		t_telephone.setText(person.getTelephone());
		TextField t_phone = new TextField();
		t_phone.setText(person.getPhone());
		TextField t_email = new TextField();
		t_email.setText(person.getEmail());
		TextField t_birthday = new TextField();
		t_birthday.setText(person.getBirthday());
		TextField t_workplace = new TextField();
		t_workplace.setText(person.getWorkplace());
		TextField t_homeaddress = new TextField();
		t_homeaddress.setText(person.getHomeaddres());
		TextField t_postcode = new TextField();
		t_postcode.setText(person.getPostcode());
		TextField t_group = new TextField();
		t_group.setText(person.getGroup());
		TextField t_note = new TextField();
		t_note.setText(person.getNote());
		t_note.setPrefHeight(100);
		Button bu1 = new Button("确定");
		Button bu2 = new Button("取消");
		Button bu3 = new Button("选择图片");
		TextField t_path = new TextField();
		t_path.setText(person.getPhotopath());
		t_path.setEditable(false);
		GridPane gr = new GridPane();
		
		bu3.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				FileChooser fileChooser = new FileChooser();// 选择文件控件
				fileChooser.getExtensionFilters().addAll(// 添加过滤器
						new ExtensionFilter("jpg文件(*.jpg)", "*.jpg"),
						new ExtensionFilter("png文件(*.png)", "*.png"),
						new ExtensionFilter("所有文件", "*.*"));
				File file = fileChooser.showOpenDialog(null);
				String path = file.getAbsolutePath();
				t_path.setText(path);
			}
		});
		
		gr.add(l_name, 0, 0);
		gr.add(t_name, 1, 0);
		gr.add(l_telephone, 0, 1);
		gr.add(t_telephone, 1, 1);
		gr.add(l_phone, 0, 2);
		gr.add(t_phone, 1, 2);
		gr.add(l_email, 0, 3);
		gr.add(t_email, 1, 3);
		gr.add(l_birthday, 0, 4);
		gr.add(t_birthday, 1, 4);
		gr.add(l_workplace, 0, 5);
		gr.add(t_workplace, 1, 5);
		gr.add(l_homeaddress, 0, 6);
		gr.add(t_homeaddress, 1, 6);
		gr.add(l_postcode, 0, 7);
		gr.add(t_postcode, 1, 7);
		gr.add(l_group, 0, 8);
		gr.add(t_group, 1, 8);
		gr.add(l_note, 0, 9);
		gr.add(t_note, 1, 9);
		gr.add(bu3, 0, 10);
		gr.add(t_path, 1, 10);
		gr.add(bu1, 0, 11);
		gr.add(bu2, 1, 11);
		gr.setMargin(bu1,new Insets(0,0,0,20));
		gr.setMargin(bu2,new Insets(0,0,0,120));
		
		bu1.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				//String people = new String();
				person.setName(t_name.getText());  
				person.setTelephone(t_telephone.getText());
				person.setPhone(t_phone.getText());
				person.setEmail(t_email.getText());
				person.setBirthday(t_birthday.getText());
				person.setWorkplace(t_workplace.getText()); 
				person.setHomeaddres(t_homeaddress.getText());
				person.setPostcode(t_postcode.getText()); 
				person.setGroup(t_group.getText());
				person.setNote(t_note.getText());
				person.setPhotopath(t_path.getText());
				information = "change";
				ModifyPerson.stage.close();
				//System.out.println(person.getGroup());
			}
		});
		bu2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				ModifyPerson.stage.close();
			}
		});
		gr.setHgap(5);//统一设置组件之间水平间距
		gr.setVgap(15);//统一设置组件之间垂直间距
		gr.setAlignment(Pos.CENTER);//设置在中间
		
		Scene scene = new Scene(gr);
		primaryStage.setScene(scene);
		primaryStage.setHeight(800);
		primaryStage.setWidth(500);
        primaryStage.setTitle("修改联系人");
        primaryStage.setResizable(false);  //不能拉伸
        primaryStage.show(); 
	}
	public void  showWindow(Person person,int index) throws Exception {
		this.person = person;
		this.index = index;
		start(stage);
	}	
	
	public static void main(String[] args) throws Exception {
		launch(args);
	}
}
