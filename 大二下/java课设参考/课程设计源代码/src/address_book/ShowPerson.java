package address_book;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ShowPerson extends Application {
	static Stage stage = new Stage();
	private Person person = new Person();

	public void start(Stage primaryStage) throws Exception {
		Label l_name = new Label("姓名:");
		Label l_telephone = new Label("电话:");
		Label l_phone = new Label("手机:");
		Label l_email = new Label("电子邮箱:");
		Label l_birthday = new Label("生日:");
		Label l_workplace = new Label("工作地:");
		Label l_homeaddress = new Label("家庭住址:");
		Label l_postcode = new Label("邮编:");
		Label l_group = new Label("组别:");
		Label l_note = new Label("备注:");

		TextField t_name = new TextField();// 名字
		t_name.setText(person.getName());
		t_name.setEditable(false);
		TextField t_telephone = new TextField();
		t_telephone.setText(person.getTelephone());
		t_telephone.setEditable(false);
		TextField t_phone = new TextField();
		t_phone.setText(person.getPhone());
		t_phone.setEditable(false);
		TextField t_email = new TextField();
		t_email.setText(person.getEmail());
		t_email.setEditable(false);
		TextField t_birthday = new TextField();
		t_birthday.setText(person.getBirthday());
		t_birthday.setEditable(false);
		TextField t_workplace = new TextField();
		t_workplace.setText(person.getWorkplace());
		t_workplace.setEditable(false);
		TextField t_homeaddress = new TextField();
		t_homeaddress.setText(person.getHomeaddres());
		t_homeaddress.setEditable(false);
		TextField t_postcode = new TextField();
		t_postcode.setText(person.getPostcode());
		t_postcode.setEditable(false);
		TextField t_group = new TextField();
		t_group.setText(person.getGroup());
		t_group.setEditable(false);
		TextField t_note = new TextField();
		t_note.setText(person.getNote());
		t_note.setEditable(false);
		t_note.setPrefHeight(100);
		Button bu1 = new Button("确定");
		Button bu2 = new Button("取消");

		GridPane gr = new GridPane();

		gr.add(l_name, 0, 0);
		gr.add(t_name, 1, 0);
		gr.add(l_telephone, 0, 1);
		gr.add(t_telephone, 1, 1);
		gr.add(l_phone, 0, 2);
		gr.add(t_phone, 1, 2);
		gr.add(l_email, 0, 3);
		gr.add(t_email, 1, 3);
		gr.add(l_birthday, 0, 4);
		gr.add(t_birthday, 1, 4);
		gr.add(l_workplace, 0, 5);
		gr.add(t_workplace, 1, 5);
		gr.add(l_homeaddress, 0, 6);
		gr.add(t_homeaddress, 1, 6); 
		gr.add(l_postcode, 0, 7);
		gr.add(t_postcode, 1, 7);
		gr.add(l_group, 0, 8);
		gr.add(t_group, 1, 8);
		gr.add(l_note, 0, 9);
		gr.add(t_note, 1, 9);
		gr.add(bu1, 0, 11);
		gr.add(bu2, 1, 11);
		gr.setMargin(bu1, new Insets(0, 0, 0, 20));
		gr.setMargin(bu2, new Insets(0, 0, 0, 120));
		
		bu1.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				ShowPerson.stage.close();
			}
		});
		bu2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				ShowPerson.stage.close();
			}
		});
		gr.setHgap(5);// 统一设置组件之间水平间距
		gr.setVgap(15);// 统一设置组件之间垂直间距
		gr.setAlignment(Pos.CENTER);// 设置在中间

		Scene scene = new Scene(gr);

		primaryStage.setScene(scene);
		primaryStage.setHeight(800);
		primaryStage.setWidth(500);
		primaryStage.setTitle("个人主页");
		primaryStage.setResizable(false); // 不能拉伸
		primaryStage.show();
	}

	public void showWindow(Person person) throws Exception {
		this.person = person;
		start(stage);
	}

	public static void main(String[] args) throws Exception {
		launch(args);
	}
}
