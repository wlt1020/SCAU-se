package address_book;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class HomePage extends VBox {
    private TextField Name = new TextField(), Phone = new TextField(), Address = new TextField();
    private GridPane gridPane = new GridPane();
    public HomePage()
    {
    	//设置不可输入
    	Name.setEditable(false);
    	Phone.setEditable(false);
    	Address.setEditable(false);
    	//设置宽度
        Name.setMaxWidth(110);
        Phone.setMaxWidth(110);
        Address.setMaxWidth(110);
        //添加行
        gridPane.addRow(20, new Label("姓名"), Name);
        gridPane.addRow(21, new Label("手机"), Phone);
        gridPane.addRow(22, new Label("地址"), Address);

        gridPane.setVgap(10);
        gridPane.setHgap(5); 
        
        setMargin(gridPane, new Insets(10));//设置外边距
        getChildren().add(gridPane);
        setSpacing(10);//设间距
        gridPane.setAlignment(Pos.CENTER);

    }
    //显示个人主页信息
    public void showHomePage(String name, String phone, String address) {
        Name.setText(name);
        Phone.setText(phone);
        Address.setText(address);
        
    }
}
