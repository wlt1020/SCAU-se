package address_book;

import java.util.ArrayList;
import java.util.List;

import ezvcard.property.Sound;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.ComboBoxListCell;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class ContackGroups extends ListView {
	//联系人组的序列
	public static final ObservableList<String> gt = FXCollections.observableArrayList();
	private ObservableList<Person> pt = null;//获取联系人
	private ListView<String> listview = new ListView<String>(gt);
	private String information = null;
	private String grname = null;//组名，查询用
	Button b1 = new Button("添加"); // 添加联系组
	Button b2 = new Button("删除"); // 联系组选择操作
	Button b3 = new Button("查询");//根据联系组查询
	TextField t_add = new TextField();//添加组输入框
    public ContackGroups() {
		
	}
	public ContackGroups(InTable in) {
		pt=in.getOlOriginalInformation();
	}
	// 添加组
	public Button AddGroup() {
		b1.setPrefSize(255, 20);
		b1.setStyle(" -fx-border-color: lightblue;-fx-background-color: lightblue");
		b1.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				String groupname = new String();
				groupname = t_add.getText();
				if (groupname != "" || groupname != null) {
					gt.add(groupname);
					t_add.setText("");
					for (int i = 0;i<gt.size();i++) {
						for (int j=i+1;j<gt.size();j++) {
							if (gt.get(i).equals(gt.get(j))) {
								gt.remove(j);
							}
						}
					}//for
					
				}//if
			}
		});
		return b1;
	}

	// 联系组删除操作
	public Button choose(InTable in) {
		b2.setPrefSize(255, 20);
		b2.setStyle(" -fx-border-color: yellow;-fx-background-color: yellow");
		b2.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				int index = listview.getSelectionModel().getSelectedIndex();
				if (index>=0) {
//					listview.getItems().get(index);
					String groupname = gt.get(index);//将要删除的
					for (int i = 0; i<pt.size();i++) {
						if (pt.get(i).getGroup().indexOf(groupname)!=-1) {
							Person person = pt.get(i);//要修改的person
							String persongroup = person.getGroup();//person中的组
							//如果是第一个元素要被删除，则删除改元素并删除其后一位的顿号
							
							if (persongroup.indexOf(groupname)==0&&persongroup.indexOf("、")>=0) {
								persongroup = persongroup.substring(groupname.length()+1);
								person.setGroup(persongroup);
								//System.out.println(persongroup);
							}
							//如果是只有一个元素要被删除，则删除改元素
							else if (persongroup.indexOf(groupname)==0 || persongroup.indexOf("、")==-1) {
								persongroup = persongroup.substring(groupname.length());
								person.setGroup(persongroup);
								//System.out.println(persongroup);
							}
							//如果不是第一个，则删除其前一个顿号（因为可能是最后一个）
							else if (persongroup.indexOf(groupname)>0){
								persongroup = persongroup.substring(0,persongroup.indexOf(groupname)-1)
										+persongroup.substring(persongroup.indexOf(groupname)+groupname.length(),persongroup.length());
								person.setGroup(persongroup);
							}
							else {
								Alert alert = new Alert(AlertType.WARNING);
								alert.setTitle("Warning");
								alert.setHeaderText("错误");
								alert.showAndWait();
							}
							pt.set(i, person);
						}
					}
					in.setOlOriginalInformation(pt); 
					//System.out.println(pt.get(0).getGroup());
					//System.out.println(in.getOlOriginalInformation().get(0).getGroup());
					gt.remove(index);
					information= "change";
				}
				else {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Warning");
					alert.setHeaderText("尚未选择");
					alert.showAndWait();
				}
			}
		});
		return b2;
	}
	public Button check() {
		b3.setPrefSize(255, 20);
		b3.setStyle(" -fx-border-color: lightgreen;-fx-background-color: lightgreen");
		b3.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if (listview.getSelectionModel().getSelectedIndex()<0) {

					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Warning");
					alert.setHeaderText("尚未选择");
					alert.showAndWait();
				}
				//index>=0
				else {
					grname = gt.get(listview.getSelectionModel().getSelectedIndex());
//					System.out.println(grname);
				}
			}
		});
		return b3;
	}
	public TextField addField() {
		return t_add;
	}
	
	public ListView showContackGroups(InTable in) {
		pt=in.getOlOriginalInformation();
		for(int i=0;i<pt.size();i++) {
			String grouptemp = pt.get(i).getGroup();
			String[] group = grouptemp.split("、");
			for (int j = 0 ;j <group.length;j++) {
				gt.add(group[j]);
			}
		}
		for (int i = 0;i<gt.size();i++) {
			for (int j=0;j<gt.size();j++) {
				if (gt.get(i).equals(gt.get(j))&&i!=j) {
					gt.remove(j);
				}
				if(gt.get(i).equals("")||gt.get(i).indexOf(" ")>=0) {
					gt.remove(j);
				}
			}
		}
		listview.setPrefSize(200, 250);
		listview.setEditable(true);
		
		return listview;
	}

	public String getInformation() {
		return information;
	}
	public void setInformation(String information) {
		this.information = information;
	}

	public ObservableList<Person> getPt() {
		return pt;
	}
	public void setPt(ObservableList<Person> pt) {
		this.pt = pt;
	}
	public String getGrname() {
		return grname;
	}
	public void setGrname(String grname) {
		this.grname = grname;
	}
	
}
