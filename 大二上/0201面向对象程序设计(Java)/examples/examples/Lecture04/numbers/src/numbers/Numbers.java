package numbers;

import java.util.Arrays;
import java.util.Scanner;

public class Numbers {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.print("输入要生成的整数的个数:");
		int n = input.nextInt();

		int[] numbers = new int[n]; // 创建长度为n的数组

		/*
		 * 使用n个范围在[0,1000)的随机整数填充数组 
		 * Math.random()返回一个[0,1)之间的随机double
		 */
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = (int) (Math.random() * 1000);
		}

		/*
		 * 使用for-each遍历数组元素并输出
		 */
		System.out.printf("生成的%d个整数如下：\n", n);
		for (int element : numbers) {
			System.out.printf("%3d ", element);
		}
		System.out.println();

		/*
		 * 使用for-each遍历数组找到最大值
		 */
		int max = numbers[0];
		for (int element : numbers) {
			if (max < element) {
				max = element;
			}
		}
		System.out.println("最大值：" + max);

		// 使用clone方法复制数组
		int[] tempNumbers = numbers.clone();
		
		//对新数组进行排序
		Arrays.sort(tempNumbers); 
		
		/*
		 * 输出排序前后的整数
		 * Arrays.toString方法把数组转换为一个字符串
		 */
		System.out.println("排序前：" + Arrays.toString(numbers));
		System.out.println("排序后：" + Arrays.toString(tempNumbers));
	}

}
