package yanghui;

import java.util.Arrays;
import java.util.Scanner;

public class Yanghui {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.print("输入生成杨辉三角的行数：");
		int numberOfRows = input.nextInt();

		// 声明并创建二维数组的行对象
		int[][] yanghui = new int[numberOfRows][];

		// 循环生成杨辉三角的每一行
		for (int row = 0; row < yanghui.length; row++) {
			// 创建第row行的列数组对象，长度为row+1
			yanghui[row] = new int[row + 1];

			// 循环生成第row的各个整数
			for (int col = 0; col < yanghui[row].length; col++) {
				if (col == 0 || col == row) {
					yanghui[row][col] = 1;
				} else {
					yanghui[row][col] = yanghui[row - 1][col - 1] + yanghui[row - 1][col];
				}
			}
		}
		
		System.out.println("二维数组的内容: ");
		// Arrays.deepToString方法可以把多维数组转换为字符串
		System.out.println(Arrays.deepToString(yanghui));
		
		// 下面演示了对二维数组进行for-each循环
		System.out.println("杨辉三角：");
		for(int[] rowOfYanghui: yanghui) {
			for(int element: rowOfYanghui) {
				System.out.printf("%4d ", element);
			}
			System.out.println();
		}

	}

}
