package primenumbers;

public class PrimeNumber {
    public static void main(String[] args) {
        final int NUMBER_OF_PRIMES = 50; // 需要显示的素数的个数
        final int NUMBER_OF_PRIMES_PER_LINE = 10; // 每行显示的素数的个数

        int count = 0; // 已经输出的素数的个数
        int number = 2; // 检测素数的起点

        System.out.printf("前%d个素数的列表：\n", NUMBER_OF_PRIMES);

        while (count < NUMBER_OF_PRIMES) {
            boolean isPrime = true; // 标记当前数是否是素数

            for (int divisor = 2; divisor <= (int) Math.sqrt(number); divisor++) {
                if (number % divisor == 0) {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime) {
                count++;

                System.out.print(number + " ");
                if (count % NUMBER_OF_PRIMES_PER_LINE == 0) {
                    System.out.println();
                }
            }

            number++; // 准备好下一个要判断的整数
        }
    }
}
