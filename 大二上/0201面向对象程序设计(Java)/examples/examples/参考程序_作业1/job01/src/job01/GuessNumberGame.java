package job01;

import java.util.Scanner;

public class GuessNumberGame {

	public static void main(String[] args) {
		boolean isContinueGame = true; // 控制进行多次游戏
		int success = 0, failure = 0; // 成功和失败的次数
		int total = 0; // 游戏总次数
		Scanner input = new Scanner(System.in);
		while (isContinueGame) {
			System.out.printf("第%d次游戏, ", ++total);
			int number = (int) (Math.random() * 10);
			System.out.println("程序生成了一个[0,9]之间的数字.");
			int n = 3;
			for (; n > 0; n--) {
				System.out.printf("你还有%d次机会, 输入你猜的数字: ", n);
				int answer = input.nextInt();
				if (answer > number) {
					System.out.println("你猜的数字太大了！");
				} else if (answer < number) {
					System.out.println("你猜的数字太小了！");
				} else {
					break;
				}
			}
			if (n > 0) {
				success++;
				System.out.println("恭喜你，猜对了!");
			} else {
				failure++;
				System.out.println("本次游戏失败！正确数字是:" + number);
			}
			System.out.print("你还要继续游戏吗？（Y继续/N退出）: ");
			isContinueGame = input.next().toUpperCase().charAt(0) == 'Y';
		}
		System.out.printf("你共进行了%d次游戏，成功%d次，失败%d次.\n", total, success, failure);
	}

}
