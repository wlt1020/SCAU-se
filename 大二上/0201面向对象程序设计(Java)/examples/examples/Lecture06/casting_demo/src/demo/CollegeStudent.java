package demo;

public class CollegeStudent extends Student {
    private String major;

    // --------------------------------------------
    public CollegeStudent(String name, String school, String major) {
        super(name, school);
        this.major = major;
    }
    
    // --------------------------------------------
    @Override
    public String sayHi() {
        return super.sayHi() + ", My major is " + major;
    }

    // --------------------------------------------
    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }
    
    
}
