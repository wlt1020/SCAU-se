package demo;

public class Teacher extends Person {
    private String cousre;

    // ---------------------------------------------------
    public Teacher(String name, String cousre) {
        super(name);
        this.cousre = cousre;
    }
    
    // ---------------------------------------------------
    @Override
    public String sayHi() {
        return super.sayHi() + " , My cousre is " + cousre;
    }
    
    // ---------------------------------------------------
    public String getCousre() {
        return cousre;
    }

    public void setCousre(String cousre) {
        this.cousre = cousre;
    }
    
    
}
