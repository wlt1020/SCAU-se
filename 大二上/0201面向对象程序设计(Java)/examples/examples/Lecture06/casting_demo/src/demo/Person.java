package demo;

public class Person {
    private String name;
    
    public Person(String name) {
        this.name = name;
    }
    // --------------------------------------------
    public String sayHi() {
        return "Hi! My Name is " + name;
    }

    // --------------------------------------------
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }   
}
