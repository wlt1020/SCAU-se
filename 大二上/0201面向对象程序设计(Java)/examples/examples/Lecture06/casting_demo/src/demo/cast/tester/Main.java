package demo.cast.tester;

import demo.CollegeStudent;
import demo.Person;
import demo.Student;
import demo.Teacher;

public class Main {

    public static void main(String[] args) {
        Person p1 = new Person("张三");
        Person p2 = new Teacher("李四", "程序设计");
        Person p3 = new Student("王五", "ABC学校");
        Person p4 = new CollegeStudent("赵六", "ABC学校", "计算机");

        Student s1 = (Student) p3;
       // Student s2 = (Student) p1; // 运行错误
       // Student s3 = (Student) p2; // 运行错误
        
        // Teacher t = new Student("王五", "ABC学校"); //语法错误

        System.out.println(p4 instanceof Person);
        System.out.println(p4 instanceof Teacher);
        System.out.println(p4 instanceof Student);
        System.out.println(p4 instanceof CollegeStudent);

        System.out.println(p4.getClass() == Student.class);
    }

}
