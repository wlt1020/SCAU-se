package demo.ploy.tester;

import demo.CollegeStudent;
import demo.Person;
import demo.Student;
import demo.Teacher;

public class Main {

    public static void main(String[] args) {
        Person p1 = new Person("张三");
        Person p2 = new Teacher("李四", "程序设计");
        Person p3 = new Student("王五", "ABC学校");
        Person p4 = new CollegeStudent("赵六", "ABC学校", "计算机");

        System.out.println(p1.sayHi());
        System.out.println(p2.sayHi());
        System.out.println(p3.sayHi());
        System.out.println(p4.sayHi());
    }

}
