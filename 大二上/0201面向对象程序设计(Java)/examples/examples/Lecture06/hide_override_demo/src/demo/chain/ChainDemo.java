package demo.chain;

class Person {
    public Person() {
        System.out.println("Person()");
    }
}

class Employee extends Person {
    public Employee() {
        this("call Employee(String)");
        System.out.println("Employee()");
    }

    public Employee(String s) {
        System.out.println(s);
    }
}

class Faculty extends Employee {
    public Faculty() {
        System.out.println("Faculty()");
    }
}

public class ChainDemo {

    public static void main(String[] args) {
    	new Faculty();
    }

}
