package demo.usesuper;

public class Child extends Parent{
    int data = 1000;
    
    @Override
    public void printData() {
        System.out.println("Child Data: " + data);
    }
    
}
