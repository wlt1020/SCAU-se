package demo.override;

public class Parent {
    int data = 100;

    public void printData() {
        System.out.println("Parent Data: " + data);
    }
}
