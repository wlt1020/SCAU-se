package demo.hide;

public class Child extends Parent{
    int data = 1000;
    
    public void printChildData() {
        System.out.println("Child Data: " + data);
    }
}
