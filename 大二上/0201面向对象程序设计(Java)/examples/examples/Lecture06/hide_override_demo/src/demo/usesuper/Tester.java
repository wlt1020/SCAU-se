package demo.usesuper;

public class Tester {

    public static void main(String[] args) {
        Parent p = new Parent();
        System.out.println(p.data);
        
        Child c = new Child();
        System.out.println(c.data);
        c.printData();
    }

}
