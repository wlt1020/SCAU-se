package demo.hide;

public class Parent {
    int data = 100;

    public void printParentData() {
        System.out.println("Parent Data: " + data);
    }
}
