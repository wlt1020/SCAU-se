package shape;

public class Circle extends Geometry {
    private double radius;

    // Constructors ---------------------------------------------
    public Circle() {
        this(1.0);
    }
    
    public Circle(double radius) {
        this.radius = radius;
    }
    
    public Circle(double radius, String color) {
        this.radius = radius;
        setColor(color);
    }
    
    // methods -------------------------------------------------
    public double getArea() {
        return Math.PI * this.radius * this.radius;
    }
    
    // setters &&��getters---------------------------------------
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
