package main;

import shape.Circle;

public class Tester {

    public static void main(String[] args) {
        Circle c1 = new Circle(100);
        c1.output();
        System.out.println(c1.getArea());
    }

}
