package shape;

import java.util.Date;

public class Geometry {
    private String color;
    private Date   dateCreated;

    // constructors -------------------------------------------------
    public Geometry() {
        this("WHITE");
    }

    public Geometry(String color) {
        this.color = color;
        this.dateCreated = new Date();
    }

    // methods-------------------------------------------------------
    public void output() {
        System.out.println("Geometry, Color: " + this.color + " , dateCreated: " + this.dateCreated);
    }

    // setters && getters--------------------------------------------
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getDateCreated() {
        return dateCreated;
    }
}
