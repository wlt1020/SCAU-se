package shape;

public class Rectangle extends Geometry {
    private double width;
    private double height;

    // constructors------------------------------------------------
    public Rectangle() {
        this(1.0, 1.0);
    }

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public Rectangle(double width, double height, String color) {
        this.width = width;
        this.height = height;
        setColor(color);
    }

    // methods -------------------------------------------------
    public double getArea() {
        return this.width * this.height;
    }

    @Override
    public void output() {
        System.out.println("Geometry, Color: " + getColor() + 
                " , dateCreated: " + getDateCreated() + 
                " , width: " + width +
                " , height: " + height);
    }

    // setters && getters------------------------------------------
    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

}
