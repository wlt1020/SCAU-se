package decoder;

public class MP3Decoder extends Decoder {

    @Override
    public String decode(String filename) {
        return String.format("MP3文件的[%s]的解码流.", filename);
    }

}
