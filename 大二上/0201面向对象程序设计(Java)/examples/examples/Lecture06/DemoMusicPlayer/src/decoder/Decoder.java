package decoder;
/**
 * 表示解码器的抽象类
 * @author xiaolei
 *
 */
public abstract class Decoder {
    /**
     * 模拟对音乐文件的解码操作的抽象方法
     * @param filename 需要解码的音乐文件名
     * @return 代表模拟的解码结果，以字符串返回
     */
    public abstract String decode(String filename);
}
