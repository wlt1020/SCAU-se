package action;

public interface Swimmable {
    public abstract void swim();
}
