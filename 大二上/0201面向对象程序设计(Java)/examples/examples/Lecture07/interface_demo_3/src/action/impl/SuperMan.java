package action.impl;

import action.Flyable;
import action.Runnable;
import action.Swimmable;

public class SuperMan implements Flyable, Runnable, Swimmable {

    @Override
    public void fly() {
        System.out.println("flying");
    }

    @Override
    public void run() {
        System.out.println("running");

    }

    @Override
    public void swim() {
        System.out.println("swimming");

    }

    public void eat() {
        System.out.println("I can eat...");
    }
}
