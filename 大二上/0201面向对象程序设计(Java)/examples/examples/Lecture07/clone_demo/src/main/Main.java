package main;

import demo.House;

public class Main {

    public static void main(String[] args) {
        House house1 = new House(1, 137.00);
        House house2 = (House)house1.clone();
        System.out.println(house1 == house2);
        System.out.println(house1.equals(house2));
        System.out.println(house1);
        System.out.println(house2);

    }

}
