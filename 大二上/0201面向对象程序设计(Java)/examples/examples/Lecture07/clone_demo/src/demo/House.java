package demo;

import java.util.Date;

public class House implements Cloneable {
    private int    id;
    private double area;
    private Date   whenBuilt;

    public House(int id, double area) {
        this.id = id;
        this.area = area;
        this.whenBuilt = new Date();
    }

    @Override
    public Object clone() {
        try {
            House t = (House) super.clone();
            t.whenBuilt = (Date) this.whenBuilt.clone();
            return t;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        House other = (House) obj;
        if (id != other.id)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "House [id=" + id + ", area=" + area + ", whenBuilt=" + whenBuilt + "]";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public Date getWhenBuilt() {
        return whenBuilt;
    }

    public void setWhenBuilt(Date whenBuilt) {
        this.whenBuilt = whenBuilt;
    }
}
