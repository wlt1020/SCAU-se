package main;

import java.util.Arrays;

import demo.Product;
import demo.ProductNameComparator;
import demo.ProductPriceComparator;

public class Main {

    public static void main(String[] args) {
        Product[] products = { 
                new Product("2001", "Dell Notebook", 6000, 200),
                new Product("2002", "MacBook Pro", 12000, 10),
                new Product("1001", "Samsung s9", 8000, 100),
                new Product("1002", "XiaoMi 8", 3000, 1000),
        };

        System.out.println("原始输入顺序输出：");
        output(products);
        
        System.out.println("按产品自然顺序(产品的id)升序输出：");
        Arrays.sort(products);
        output(products);
        
        System.out.println("按产品的价格升序输出：");
        Arrays.sort(products, new ProductPriceComparator());
        output(products);

        System.out.println("按产品的名称升序输出：");
        Arrays.sort(products, new ProductNameComparator());
        output(products);
    }

    private static void output(Product[] products) {
        for(Product product: products) {
            System.out.println(product);
        }
    }
}
