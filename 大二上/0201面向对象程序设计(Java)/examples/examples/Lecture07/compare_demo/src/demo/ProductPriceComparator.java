package demo;

import java.util.Comparator;

/**
 * 按产品的价格实现的比较器类 
 *
 */
public class ProductPriceComparator implements Comparator<Product> {
    @Override
    public int compare(Product p1, Product p2) {
        return Double.compare(p1.getPrice(), p2.getPrice());
    }
}
