package demo;

public class Product implements Comparable<Product> {
    private String id;
    private String name;
    private double price;
    private int    saleVolume;

    // Constructor -----------------------------------------------------
    public Product(String id, String name, double price, int saleVolume) {
        super();
        this.id = id;
        this.name = name;
        this.price = price;
        this.saleVolume = saleVolume;
    }

    // 实现接口的抽象方法，比较依据是id
    @Override
    public int compareTo(Product other) {
        return this.id.compareTo(other.id);
    }

    @Override
    public String toString() {
        return String.format("%-10s|%-20s|%8.2f|%4d", id,name,price, saleVolume);
    }

    // setters && getters -------------------------------------------
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSaleVolume() {
        return saleVolume;
    }

    public void setSaleVolume(int saleVolume) {
        this.saleVolume = saleVolume;
    }
}
