package demo;

import java.util.Comparator;

/**
 * 按产品的名称实现的比较器类 
 * 
 */
public class ProductNameComparator implements Comparator<Product> {
    @Override
    public int compare(Product p1, Product p2) {
        return p1.getName().compareTo(p2.getName());
    }
}
