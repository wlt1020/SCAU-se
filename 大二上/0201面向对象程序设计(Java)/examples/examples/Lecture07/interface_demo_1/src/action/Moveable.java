package action;

public interface Moveable {
    public static final int MAX_SPEED = 10000;
    
    public abstract void run();
    public abstract void fly();
}
