package action;

public class SuperMan implements Moveable {

    @Override
    public void run() {
        System.out.println("running...");
    }

    @Override
    public void fly() {
        System.out.println("flying...");
    }

}
