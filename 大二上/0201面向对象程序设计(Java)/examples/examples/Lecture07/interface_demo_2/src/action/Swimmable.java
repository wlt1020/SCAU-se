package action;

public interface Swimmable {
    public  final int MAX_SPEED = 1000;
    public abstract void run();
}
