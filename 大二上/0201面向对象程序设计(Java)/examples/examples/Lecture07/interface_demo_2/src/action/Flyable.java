package action;

public interface Flyable {
    public  final int MAX_SPEED = 1000;
    public abstract void fly();
}
