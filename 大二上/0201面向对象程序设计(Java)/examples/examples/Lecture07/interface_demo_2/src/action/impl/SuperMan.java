package action.impl;

import action.Moveable;

public class SuperMan implements Moveable {

    @Override
    public void fly() {
        System.out.println("flying");
    }

    @Override
    public void run() {
        System.out.println("running");
    }
    
    public void run(String s) 
    {
    	System.out.println(s+" is running");
		
	}

    public void swim() {
        System.out.println("swimming");

    }

}
