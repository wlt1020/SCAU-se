package action;

public interface Runnable {
    public  final int MAX_SPEED = 1000;
    public abstract void run();
}
