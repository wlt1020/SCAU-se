//ComputeAreaWithCircle.java
import java.util.Scanner;
public class ComputeAreaWithCircle {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //创建一个用于从键盘输入的Scanner对象

		Circle firstCircle = new Circle();      //创建第一个Circle对象
		System.out.print("输入第一个圆的半径: ");
		firstCircle.radius = input.nextDouble();
		System.out.println("第一个圆的面积: " + firstCircle.getArea());

		Circle secondCircle = new Circle();     //创建第二个Circle对象
		System.out.print("输入第二个圆的半径: ");
		secondCircle.radius = input.nextDouble();
		System.out.println("第二个圆的面积: " + secondCircle.getArea());
	}
} 