package shape;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		Rectangle rectangle = new Rectangle();
		System.out.println("输入矩形的宽和高: ");
		rectangle.width = input.nextDouble();
		rectangle.height = input.nextDouble();
		System.out.println("面积: " + rectangle.getArea());
		
		Lader lader = new Lader();
		System.out.println("输入梯形的上底、下底和高: ");
		lader.above = input.nextDouble();
		lader.bottom = input.nextDouble();
		lader.height = input.nextDouble();
		System.out.println("面积: " + lader.getArea());
	}
}
