package shape;

public class Lader {

	double above;  // 梯形的上底  
	double bottom; // 梯形的下底
	double height; // 梯形的高

	/**
	 * 计算梯形的面积
	 * @return 当前梯形对角的面积
	 */
	double getArea() {
		return (above + bottom) * height / 2;
	}
}
