package ex02;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("输入第1个圆 x y r : ");
		double x1 = input.nextDouble();
		double y1 = input.nextDouble();
		double r1 = input.nextDouble();
		System.out.print("输入第2个圆 x y r : ");
		double x2 = input.nextDouble();
		double y2 = input.nextDouble();
		double r2 = input.nextDouble();

		double distance = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
		if (distance <= Math.abs(r1 - r2)) {
			if (r1 > r2) {
				System.out.printf("(%.2f,%.2f)-%.2f 包含 (%.2f,%.2f)-%.2f\n", x1, y1, r1, x2, y2, r2);
			} else {
				System.out.printf("(%.2f,%.2f)-%.2f 包含 (%.2f,%.2f)-%.2f\n", x2, y2, r2, x1, y1, r1);
			}
		} else if (distance >= (r1 + r2)) {
			System.out.printf("(%.2f,%.2f)-%.2f 与 (%.2f,%.2f)-%.2f 无关\n", x1, y1, r1, x2, y2, r2);
		} else {
			System.out.printf("(%.2f,%.2f)-%.2f 与 (%.2f,%.2f)-%.2f 相交\n", x1, y1, r1, x2, y2, r2);
		}
	}

}
