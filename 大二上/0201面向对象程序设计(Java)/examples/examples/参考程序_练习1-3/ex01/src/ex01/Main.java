package ex01;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
        System.out.print("input a integer(0<=n<1000):");
        int n = input.nextInt();
        int sum = n%10 + n/10%10 + n/100;
        System.out.printf("The sum of the digits of %d is %d \n", n, sum);
	}

}
