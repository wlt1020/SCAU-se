package parser;

public class StringStack {
	private String[] elements;
	private int size;

	public StringStack() {
		this.elements = new String[16];
		this.size = 0;
	}

	public void push(String e) {
		if (this.size == this.elements.length) {
			String[] temp = new String[this.elements.length + 16];
			System.arraycopy(this.elements, 0, temp, 0, this.elements.length);
			this.elements = temp;
		}
		this.elements[size++] = e;
	}

	public String pop() {
		return this.elements[--size];
	}

	public String peek() {
		return this.elements[size - 1];
	}

	public boolean isEmpty() {
		return this.size == 0;
	}

	public int getSize() {
		return size;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("[");
		for (int i = size - 1; i >= 0; i--) {
			builder.append(elements[i]);
			builder.append(" ");
		}
		builder.append("]");
		return builder.toString();
	}
}
