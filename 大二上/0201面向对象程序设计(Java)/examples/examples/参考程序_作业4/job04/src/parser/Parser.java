package parser;

public class Parser {
	private static final String OPERATOR_REGEX = "[\\+\\-\\*/]"; 
	private StringStack operatorStack;
	private StringStack operandStack;

	public Parser() {
		this.operandStack = new StringStack();
		this.operatorStack = new StringStack();
	}

	public int parser(String expression) {
		String[] opers = pretreatment(expression); // 预处理
		for (String oper : opers) {
			if (oper.matches("[\\+\\-]")) { // 如果是运算符+或-
				while (!operatorStack.isEmpty() && (operatorStack.peek().matches(OPERATOR_REGEX))) {
					processOneOperator();
				}
				operatorStack.push(oper); // 当前运算符入栈
			} else if (oper.matches("[\\*/]")) {// 如果是运算符*或/
				while (!operatorStack.isEmpty() && (operatorStack.peek().matches("[\\*/]"))) {
					processOneOperator();
				}
				operatorStack.push(oper); // 当前运算符入栈
			} else if ("(".equals(oper)) { // 如果是(
				operatorStack.push(oper);
			} else if (")".equals(oper)) { // 如果是)
				while (!"(".equals(operatorStack.peek())) {
					processOneOperator();
				}
				operatorStack.pop();  //弹出(
			} else { // 如果是运算数
				operandStack.push(oper);
			}
//			System.out.println();
//			System.out.println(operandStack);
//			System.out.println(operatorStack);
		}

		while (!operatorStack.isEmpty()) {
			processOneOperator();
//			System.out.println(operandStack);
//			System.out.println(operatorStack);
		}

		return Integer.parseInt(operandStack.pop());

	}

	/**
	 * 对四则运算表达式进行预处理，将表达式转换为一个数组，数组的元素按照表达式中运算数和运算符原始顺序排列
	 * @param expression 表达式
	 * @return 转换后的数组 
	 */
	private String[] pretreatment(String expression) {
		expression = expression.replaceAll(" +", ""); // 去掉表达式中所有空格
		
		expression = expression.replaceAll(" +", "");
		expression = expression.replaceAll("\\*", " * ");
		expression = expression.replaceAll("\\+", " + ");
		expression = expression.replaceAll("\\-", " - ");
		expression = expression.replaceAll("/", " / ");
		expression = expression.replaceAll("\\(", "( ");
		expression = expression.replaceAll("\\)", " )");

//		System.out.println(expression);
		String[] result = expression.split(" ");
		return result;
	}

	/**
	 * 对运算符栈顶的一个运算符进行运算，运算结果进入运算数栈顶
	 */
	private void processOneOperator() {
		String operator = operatorStack.pop(); // 弹出栈顶运算符
		int operand1 = Integer.parseInt(operandStack.pop()); // 弹出栈顶运算数并转换为int
		int operand2 = Integer.parseInt(operandStack.pop()); // 弹出栈顶运算数并转换为int

		int result = 0;
		switch (operator) {
		case "+":
			result = operand2 + operand1;
			break;
		case "-":
			result = operand2 - operand1;
			break;
		case "*":
			result = operand2 * operand1;
			break;
		case "/":
			result = operand2 / operand1;
			break;
		}
		operandStack.push(String.valueOf(result)); // 运算结果转换成String并入栈
	}
}
