package main;

import java.util.Arrays;
import java.util.Scanner;

import parser.Parser;

public class Main {

    public static void main(String[] args) {
        String exp = null;

        Scanner input = new Scanner(System.in);
        exp = input.nextLine();
        String[] array = exp.split(" ");
        System.out.println(Arrays.toString(array));
        
        Parser parser = new Parser();

        System.out.println(exp + " = ");
        System.out.println(parser.parser(exp));

    }

}
