package manager;

import java.util.ArrayList;

import bank.Account;
import bank.CreditAccount;
import bank.SavingAccount;

public class AccountManager {
	private ArrayList<Account> accounts;

	public AccountManager() {
		accounts = new ArrayList<>();
	}

	public boolean addAccount(Account account) {
		boolean found = false;
		for (Account item : accounts) {
			if (item.getId().equals(account.getId())) {
				found = true;
				break;
			}
		}
		if (!found) {
			accounts.add(account);
		}
		return !found;
	}

	public Account getAccount(String id) {
		Account result = null;
		for (Account account : accounts) {
			if (account.getId().equals(id)) {
				result = account;
				break;
			}
		}
		return result;
	}

	public boolean removeAccount(Account account) {
		return accounts.remove(account);
	}

	public double getTotalBalance() {
		double result = 0;
		for (Account account : accounts) {
			result += account.getBalance();
		}
		return result;
	}

	public double getTotalOverdraft() {
		double result = 0;
		for (Account account : accounts) {
			if (account instanceof CreditAccount) {
				CreditAccount creditAccount = (CreditAccount) account;
				if (creditAccount.getBalance() < 0) {
					result += creditAccount.getBalance();
				}
			}
		}
		return result;
	}
	
	public int getNumberOfAccount() {
		return accounts.size();
	}
	
	public int getNumberOfCreditAccount() {
		int result = 0;
		for (Account account : accounts) {
			if(account instanceof CreditAccount) {
				result++;
			}
		}
		return result;
	}

	public int getNumberOfSavingAccount() {
		int result = 0;
		for (Account account : accounts) {
			if(account instanceof SavingAccount) {
				result++;
			}
		}
		return result;
	}
	
	public Account[] toArray() {
		Account[] result = new Account[accounts.size()];
		for(int i=0; i<result.length; i++) {
			result[i] = accounts.get(i);
		}
		return result;
	}
}
