package ui;

import java.util.Scanner;

import bank.Account;
import bank.CreditAccount;
import bank.SavingAccount;
import manager.AccountManager;

public class BankUI {
	private AccountManager manager;

	public BankUI() {
		manager = new AccountManager();
		initAccounts(10);
	}

	public void displayMenu() {
		Scanner input = new Scanner(System.in);
		boolean menuDisplayed = true;
		do {
			System.out.println("----Menu----");
			System.out.println("1.增加账户");
			System.out.println("2.删除账户");
			System.out.println("3.存款操作");
			System.out.println("4.取款操作");
			System.out.println("5.数据统计");
			System.out.println("6.账户列表");
			System.out.println("0.退出");
			System.out.println("------------");
			System.out.print("请选择: ");
			char choice = input.next().charAt(0);
			switch (choice) {
			case '1':
				add();
				break;
			case '2':

				break;
			case '3':

				break;
			case '4':

				break;
			case '5':

				break;
			case '6':
				listAll();
				break;
			case '0':
				menuDisplayed = false;
				break;
			default:
				break;
			}

		} while (menuDisplayed);
	}

	private void initAccounts(int n) {
		for (int i = 0; i < n; i++) {
			int rand = (int) (Math.random() * 100);

			String id = String.format("%012d", rand);
			String name = String.format("Name%04d", rand);
			Account account = null;
			if (rand % 2 == 0) {
				account = new SavingAccount(id, name);
			} else {
				account = new CreditAccount(id, name, 1000);
			}
			manager.addAccount(account);
		}
	}

	private void add() {
		System.out.println("增加账户-------------------");
		Scanner input = new Scanner(System.in);
		System.out.print("账户类别(1-信用卡 2-储蓄卡):");
		char type = input.next().charAt(0);
		System.out.println(type);
		if (type != '1' && type != '2') {
			System.out.println("账户类别输入错误！");
			return;
		}
		
		System.out.print("账号：");
		String id = input.next();
		System.out.print("姓名：");
		String name = input.next();
		Account account = null;
		if (type == '1') {
			account = new CreditAccount(id, name, 1000);
		} else if (type == '2') {
			account = new SavingAccount(id, name);
		}
		
		if(manager.addAccount(account)) {
			System.out.println("账户增加成功！");
		}else {
			System.out.println("账户增加失败， 账号已经被使用！");
		}

	}

	private void listAll() {
		Account[] accounts = manager.toArray();
		System.out.println("全部账户--------");
		for (Account account : accounts) {
			System.out.println(account);
		}
		System.out.println("-------------");
	}
}
