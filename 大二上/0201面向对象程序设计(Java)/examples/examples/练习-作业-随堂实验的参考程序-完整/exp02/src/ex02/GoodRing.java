package ex02;

/**
 * <pre>
 * 回答问题：
 *  (1) 构造方法名与类名相同，因此需要修改为 GoodRing, 使用public较为合适 
 *  (2)3个数据域使用private修饰，对应的访问器和修改器使用public修饰 
 *  (3) 计算方法使用 public 修饰
 * </pre>
 */
public class GoodRing {
	private double innerRadius;
	private double outerRadius;
	private String fillColor;

	public GoodRing() {
		innerRadius = 1;
		outerRadius = 2;
		fillColor = "WHITE";
	}

	public GoodRing(double iRadius, double oRadius, String color) {
		innerRadius = iRadius;
		outerRadius = oRadius;
		fillColor = color;
	}

	public double getArea() {
		return Math.PI * (outerRadius * outerRadius - innerRadius * innerRadius);
	}

	public double getInnerPerimeter() {
		return 2 * Math.PI * innerRadius;
	}

	public double getOuterPerimeter() {
		return 2 * Math.PI * outerRadius;
	}

	/**
	 * @return the innerRadius
	 */
	public double getInnerRadius() {
		return innerRadius;
	}

	/**
	 * @param innerRadius the innerRadius to set
	 */
	public void setInnerRadius(double innerRadius) {
		this.innerRadius = innerRadius;
	}

	/**
	 * @return the outerRadius
	 */
	public double getOuterRadius() {
		return outerRadius;
	}

	/**
	 * @param outerRadius the outerRadius to set
	 */
	public void setOuterRadius(double outerRadius) {
		this.outerRadius = outerRadius;
	}

	/**
	 * @return the fillColor
	 */
	public String getFillColor() {
		return fillColor;
	}

	/**
	 * @param fillColor the fillColor to set
	 */
	public void setFillColor(String fillColor) {
		this.fillColor = fillColor;
	}
}
