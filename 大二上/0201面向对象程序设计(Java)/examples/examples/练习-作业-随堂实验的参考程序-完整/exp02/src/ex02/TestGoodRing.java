package ex02;

import java.util.Scanner;

public class TestGoodRing {

	public static void main(String[] args) {
		GoodRing ring = new GoodRing();

		System.out.println("内圆半径：" + ring.getInnerRadius());
		System.out.println("外圆半径：" + ring.getOuterRadius());
		System.out.println("填充颜色：" + ring.getFillColor());
		System.out.println("环的面积：" + ring.getArea());
		System.out.println("内圆周长：" + ring.getInnerPerimeter());
		System.out.println("外圆周长：" + ring.getOuterPerimeter());

		ring.setInnerRadius(5);
		ring.setOuterRadius(10);
		ring.setFillColor("RED");

		System.out.println("内圆半径：" + ring.getInnerRadius());
		System.out.println("外圆半径：" + ring.getOuterRadius());
		System.out.println("填充颜色：" + ring.getFillColor());
		System.out.println("环的面积：" + ring.getArea());
		System.out.println("内圆周长：" + ring.getInnerPerimeter());
		System.out.println("外圆周长：" + ring.getOuterPerimeter());

		System.out.println("--------------------");
		Scanner input = new Scanner(System.in);
		System.out.print("输入创建环的个数:");
		int n = input.nextInt();

		GoodRing[] rings = GoodRingUtil.createGoodRings(n);

		System.out.println("面积之和：" + GoodRingUtil.totalArea(rings));

		System.out.println("排序之前: ");
		GoodRingUtil.outputGoodRings(rings);

		GoodRingUtil.sort(rings);

		System.out.println("排序之后: ");
		GoodRingUtil.outputGoodRings(rings);

	}

}
