package ex02;

public class GoodRingUtil {
	public static GoodRing[] createGoodRings(int n) {
		GoodRing[] rings = new GoodRing[n];
		for (int i = 0; i < rings.length; i++) {
			double ir = Math.random() * 100;
			double or = Math.random() * 100;
			if (ir > or) {
				double temp = ir;
				ir = or;
				or = temp;
			}

			rings[i] = new GoodRing(ir, or, "WHITE");
		}
		return rings;
	}

	public static double totalArea(GoodRing[] rings) {
		double result = 0;
		for (GoodRing ring : rings) {
			result += ring.getArea();
		}
		return result;
	}

	public static void sort(GoodRing[] rings) {
		for (int i = 0; i < rings.length - 1; i++) {
			int indexOfMax = i;
			for (int j = i + 1; j < rings.length; j++) {
				if (rings[j].getArea() > rings[indexOfMax].getArea()) {
					indexOfMax = j;
				}
			}
			if (i != indexOfMax) {
				GoodRing temp = rings[i];
				rings[i] = rings[indexOfMax];
				rings[indexOfMax] = temp;
			}
		}
	}

	public static void outputGoodRings(GoodRing[] rings) {
		for (GoodRing ring : rings) {
			System.out.printf("%8.2f, %8.2f, %-10s, %10.2f\n", ring.getInnerRadius(), ring.getOuterRadius(),
					ring.getFillColor(), ring.getArea());
		}
	}
}
