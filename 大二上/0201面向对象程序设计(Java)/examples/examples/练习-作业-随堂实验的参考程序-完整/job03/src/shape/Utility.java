package shape;

public class Utility {
	public static int compare(Rectangle rect1, Rectangle rect2) {
		int result = 0;
		if (rect1.getArea() > rect2.getArea()) {
			result = 1;
		}
		if (rect1.getArea() < rect2.getArea()) {
			result = -1;
		}
		return result;
	}

	public static void sort(Rectangle[] rectangles) {
		for (int i = 0; i < rectangles.length - 1; i++) {
			for (int j = 0; j < rectangles.length - 1 - i; j++) {
				if (rectangles[j].getArea() < rectangles[j + 1].getArea()) {
					Rectangle temp = rectangles[j];
					rectangles[j] = rectangles[j + 1];
					rectangles[j + 1] = temp;
				}
			}
		}
	}

	public static void output(Rectangle[] rectangles) {
		for (Rectangle rect : rectangles) {
			System.out.printf("[%6.2f,%6.2f]-%10.2f\n", rect.getWidth(), rect.getHeight(), rect.getArea());
		}
	}
}
