package job05.shape;

/**
 * @author xiaolei 矩形类
 */
public class Rectangle implements AreaCalculator {
	private double width;
	private double height;

	// 构造方法 ---------------------------------------------

	public Rectangle(double width, double height) {
		this.width = width;
		this.height = height;
	}

	public Rectangle() {
		this(0.0, 0.0);
	}

	// 重写接口的抽象方法-------------------------------------
	@Override
	public double getArea() {
		return this.width * this.height;
	}

	// set & get methods ------------------------------------
	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

}
