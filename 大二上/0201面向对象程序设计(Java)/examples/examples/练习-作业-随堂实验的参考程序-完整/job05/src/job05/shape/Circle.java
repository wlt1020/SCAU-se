package job05.shape;

/**
 * @author xiaolei
 * 圆类
 */
public class Circle implements AreaCalculator {
	private double radius; 

	// 构造方法 ----------------------------------------
	public Circle(double radius) {
		this.radius = radius;
	}

	public Circle() {
		this(0.0);
	}

	// 重写接口的抽象方法 -------------------------------
	@Override
	public double getArea() {
		return Math.PI * radius * radius;
	}

	// set & get methods ------------------------------
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	
}
