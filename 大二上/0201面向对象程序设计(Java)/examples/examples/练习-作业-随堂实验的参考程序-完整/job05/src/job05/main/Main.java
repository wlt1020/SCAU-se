package job05.main;

import java.awt.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import job05.shape.Circle;
import job05.shape.Lader;
import job05.shape.Rectangle;
import job05.util.UtilTotalArea;

public class Main {
	public static Scanner scanner = new Scanner(System.in);

	public static ArrayList<Object> inputShapes() {
		ArrayList<Object> shapes = new ArrayList<>();
		while(true) {
			String row = scanner.nextLine();
			if (row.trim().length() == 0) break;
			String[] data = row.split("[ ]+");
			Object obj = null;
			switch (data[0]) {
			case "A":
				obj = new Rectangle(Double.parseDouble(data[1]), Double.parseDouble(data[2]));
				break;
			case "B":
				obj = new Circle(Double.parseDouble(data[1]));
				break;
			case "C":
				obj = new Lader(Double.parseDouble(data[1]), Double.parseDouble(data[2]), Double.parseDouble(data[3]));
				break;
			}
			shapes.add(obj);
		} 
		return shapes;
	}

	public static void main(String[] args) {
		String row = scanner.nextLine(); // 读取输入第一行
		String[] data = row.split("[ ]{1,}");
		double areaOfPane = Double.parseDouble(data[0]) * Double.parseDouble(data[1]);

		ArrayList<Object> shapes = inputShapes();
		double sumAreaOfShape = UtilTotalArea.totalArea(shapes);

		System.out.printf("%.2f%%\n", sumAreaOfShape * 100 / areaOfPane);
	}

}
