package job05.util;

import java.util.ArrayList;

import job05.shape.AreaCalculator;

/**
 *  @author xiaolei
 *  计算数组中面积之和的工具类
 */
public class UtilTotalArea {
	// 私有构造方法的作用
	private UtilTotalArea() {
	}

	public static double totalArea(ArrayList<Object> shapes) {
		double reuslt = 0.0;
		for (Object obj : shapes) {
			reuslt += ((AreaCalculator) obj).getArea();
		}
		return reuslt;
	}
}
