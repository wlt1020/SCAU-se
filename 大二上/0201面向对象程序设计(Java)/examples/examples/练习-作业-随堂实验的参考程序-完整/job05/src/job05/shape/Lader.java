package job05.shape;

/**
 * @author xiaolei 梯形类
 */
public class Lader implements AreaCalculator {
	private double top;
	private double bottom;
	private double height;

	// 构造方法 -------------------------------------------------
	public Lader() {
		this(0.0, 0.0, 0.0);
	}

	public Lader(double top, double bottom, double height) {
		this.top = top;
		this.bottom = bottom;
		this.height = height;
	}

	// 重写接口的抽象方法 -----------------------------------------
	@Override
	public double getArea() {
		return (top + bottom) * height / 2;
	}

	// set & get methods -----------------------------------------
	public double getTop() {
		return top;
	}

	public void setTop(double top) {
		this.top = top;
	}

	public double getBottom() {
		return bottom;
	}

	public void setBottom(double bottom) {
		this.bottom = bottom;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

}
