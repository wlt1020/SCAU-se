package job05.shape;

/**
 * 定义计算面积的抽象方法的接口 
 * @author xiaolei
 *
 */
public interface AreaCalculator {
	public double getArea();
}
