package data;

public class Product implements Comparable<Product>, Cloneable {
	private String id; // 编号
	private String name; // 名称
	private double price; // 价格
	private int saleVolumn; // 销量

	// 构造方法 -------------------------------------------
	public Product(String id, String name, double price, int saleVolumn) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.saleVolumn = saleVolumn;
	}

	// 重写 java.lang.Comparable 接口的抽象方法 ---------------------
	@Override
	public int compareTo(Product other) {
		// 按产品的id比较大小
		return this.id.compareTo(other.id);
	}

	// 重写 java.lang.Object 的clone方法, 注意这是 浅克隆 ------------
	@Override
	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}

	// 重写 java.lang.Object 的hashCode方法 ------------------------
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	// 重写 java.lang.Object 的equals方法, 判断依据与compareTo方法相同 ------------
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (this.id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("%-10s %-20s %10.2f %10d", id, name, price, saleVolumn);
	}

	// set & get ------------------------------------------
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getSaleVolumn() {
		return saleVolumn;
	}

	public void setSaleVolumn(int saleVolumn) {
		this.saleVolumn = saleVolumn;
	}

}
