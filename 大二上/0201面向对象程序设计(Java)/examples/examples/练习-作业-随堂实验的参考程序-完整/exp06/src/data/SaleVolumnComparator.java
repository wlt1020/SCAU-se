package data;

import java.util.Comparator;

public class SaleVolumnComparator implements Comparator<Product> {

	@Override
	public int compare(Product p1, Product p2) {
		// 按产品的销量比较
		return p1.getSaleVolumn() - p2.getSaleVolumn();
	}

}
