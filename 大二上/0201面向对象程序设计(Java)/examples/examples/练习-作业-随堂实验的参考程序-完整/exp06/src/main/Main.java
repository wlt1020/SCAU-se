package main;

import java.util.Arrays;

import data.PriceComparator;
import data.Product;
import data.SaleVolumnComparator;

public class Main {
	public static String ArrayToString(Object[] array) {
		StringBuilder builder = new StringBuilder();
		for(Object obj:array) {
			builder.append(obj.toString());
			builder.append("\n");
		}
		return builder.toString();
	}

	public static void main(String[] args) {
		System.out.println("测试克隆的示例:");
		Product p1 = new Product("1001", "笔记本电脑", 8000.0, 10);
		Product p2 = (Product) p1.clone();

		System.out.println("p1 == p2 的结果: " + (p1 == p2));
		// 注意: 下面这个结果与Product有无重写equals方法及怎样重写有关
		System.out.println("p1.equals(p2)的结果: " + p1.equals(p2));
		
		System.out.println("测试排序的示例:");
		// 定义并初始化一个Product数组
		Product[] products = {
				new Product("2001", "Notebook Computer", 8000.0, 10),
				new Product("2002", "Desktop Computer", 4000.0, 20),
				new Product("1001", "Mobile", 6000.0, 30),
				new Product("1002", "Camera", 7000.0, 15),
		};
		
		// 使用默认比较排序, 使用Comparable接口
		Arrays.sort(products);
		System.out.println("按ID排序：");
		System.out.println(ArrayToString(products));
		
		// 使用价格比较排序, 使用PriceComparator
		Arrays.sort(products, new PriceComparator());
		System.out.println("按价格排序：");
		System.out.println(ArrayToString(products));
		                      
		// 使用销量比较排序, 使用SalevolumnComparator
		Arrays.sort(products, new SaleVolumnComparator());
		System.out.println("按销量排序：");
		System.out.println(ArrayToString(products));
	}

}
