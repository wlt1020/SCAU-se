package bank;

public class CreditAccount extends Account {
	private double limit;

	// Constructor --------------------------------------------
	public CreditAccount(String id, String name, double limit) {
		this(id, name, 0.0, limit);
	}

	public CreditAccount(String id, String name, double balance, double limit) {
		super(id, name, balance);
		this.limit = limit;
	}

	//
	@Override
	public boolean withdraw(double amount) {
		if (amount > this.getBalance() + limit) {
			return false;
		} else {
			return super.withdraw(amount);
		}
	}

	@Override
	public String toString() {
		return super.toString() + " , Limit: ��" + this.limit;
	}

	// Setters && Getters -------------------------------------
	public double getLimit() {
		return limit;
	}

	public void setLimit(double limit) {
		this.limit = limit;
	}

}
