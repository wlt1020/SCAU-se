package bank;

public class SavingAccount extends Account {
	
	public SavingAccount(String id, String name) {
		this(id, name, 0.0);
	}

	public SavingAccount(String id, String name, double balance) {
		super(id, name, balance);
	}

	@Override
	public boolean withdraw(double amount) {
		if (amount > this.getBalance()) {
			return false;
		} else {
			return super.withdraw(amount);
		}
	}

}
