package bank;

public class Account {
	private String id;
	private String name;
	private double balance;

	// Constructor --------------------------------------------
	public Account(String id, String name) {
		this(id, name, 0.0);
	}

	public Account(String id, String name, double balance) {
		this.id = id;
		this.name = name;
		this.balance = balance;
	}

	//
	public void deposit(double amount) {
		this.balance += amount;
	}

	public boolean withdraw(double amount) {
		this.balance -= amount;
		return true;
	}

	@Override
	public String toString() {
		return "[" + id + "," + name + "] - ��" + balance;
	}

	// Setter && Getter----------------------------------------
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public double getBalance() {
		return balance;
	}

}
