package main;

import bank.CreditAccount;
import bank.SavingAccount;

public class Main {

	public static void main(String[] args) {
		System.out.println("信用卡账号测试:");
		CreditAccount credit = new CreditAccount("0001", "张三", 1000);
		System.out.println(credit);
		credit.deposit(1500);
		System.out.println("存入1500.00，" + credit);
		if(credit.withdraw(800)) {
			System.out.println("取出800.00成功，" + credit);			
		}else {
			System.out.println("取出800.00失败，" + credit);						
		}
		if(credit.withdraw(800)) {
			System.out.println("取出800.00成功，" + credit);			
		}else {
			System.out.println("取出800.00失败，" + credit);						
		}
		if(credit.withdraw(800)) {
			System.out.println("取出800.00成功，" + credit);			
		}else {
			System.out.println("取出800.00失败，" + credit);						
		}
		if(credit.withdraw(800)) {
			System.out.println("取出800.00成功，" + credit);			
		}else {
			System.out.println("取出800.00失败，" + credit);						
		}
		
		
		System.out.println("储蓄卡账号测试:");
		SavingAccount saving = new SavingAccount("0001", "张三");
		System.out.println(saving);
		saving.deposit(1500);
		System.out.println("存入1500.00，" + saving);
		if(saving.withdraw(800)) {
			System.out.println("取出800.00成功，" + saving);			
		}else {
			System.out.println("取出800.00失败，" + saving);						
		}
		if(saving.withdraw(800)) {
			System.out.println("取出800.00成功，" + saving);			
		}else {
			System.out.println("取出800.00失败，" + saving);						
		}
		if(saving.withdraw(800)) {
			System.out.println("取出800.00成功，" + saving);			
		}else {
			System.out.println("取出800.00失败，" + saving);						
		}
		
		

	}

}
