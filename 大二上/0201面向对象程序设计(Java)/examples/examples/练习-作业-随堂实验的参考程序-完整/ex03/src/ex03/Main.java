package ex03;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("输入年份: ");
		int year = input.nextInt();
		System.out.print("该年第1天是星期几: ");
		int week = input.nextInt();

		for (int i = 1; i <= 12; i++) {
			System.out.printf("%4d年%2d月\n", year, i);
			System.out.println("---------------------------");
			System.out.println("Sun Mon Tue Wed Thu Fri Sat");
			for (int d = 0; d < week; d++) {
				System.out.print("    ");
			}
			for (int d = 1; d <= getDays(year, i); d++) {
				System.out.printf("%3d ", d);
				week = (week + 1) % 7;
//				System.out.println(week);
				if (week == 0) {
					System.out.println();
				}
			}
			System.out.println("\n---------------------------");
//			week = (week + 1) % 7;
		}
	}

	public static int getDays(int year, int month) {
		int days = 0;
		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			days = 31;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			days = 30;
			break;
		case 2:
			if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
				days = 29;
			} else {
				days = 28;
			}

		}
		return days;
	}
}
