package ex01;

/*
 * 思考题1：
 * innerRadius和outerRadius的初始默认值为0.0
 * fillColor的初始默认值为null
 * 
 * 构造方法不能有返回类型
 * 
 * 
 */
public class Ring {
	double innerRadius;
	double outerRadius;
	String fillColor;

	Ring() {
		innerRadius = 1;
		outerRadius = 2;
		fillColor = "WHITE";
	}

	Ring(double iRadius, double oRadius, String color) {
		innerRadius = iRadius;
		outerRadius = oRadius;
		fillColor = color;
	}

	double getArea() {
		return Math.PI * (outerRadius * outerRadius - innerRadius * innerRadius);
	}

	double getInnerPerimeter() {
		return 2 * Math.PI * innerRadius;
	}
	
	double getOuterPerimeter() {
		return 2 * Math.PI * outerRadius;
	}
}
