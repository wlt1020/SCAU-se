package ex01;

/**
 * 思考题2：
 * 进行语句替换后出现编译错误：The Constructor Ring() is undefined.
 * 
 * 思考题3：
 * 为Ring类添加无参构造方法Ring()后，编译没有错误。
 * 两个构造方法是 重载（Overload）关系。
 *
 */
public class TestRing {

	public static void main(String[] args) {
		Ring ring = new Ring();
		System.out.println("内圆半径："+ring.innerRadius);
		System.out.println("外圆半径："+ring.outerRadius);
		System.out.println("填充颜色："+ring.fillColor);
		System.out.println("环的面积："+ring.getArea());
		System.out.println("内圆周长："+ring.getInnerPerimeter());
		System.out.println("外圆周长："+ring.getOuterPerimeter());
	}

}
