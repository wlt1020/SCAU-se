package ex03;

public class Circles {
	public static Circle[] createCircles(int n) {
		Circle[] circles = new Circle[n];
		for (int i = 0; i < circles.length; i++) {
			int x = (int) (Math.random() * 100);
			int y = (int) (Math.random() * 100);
			double r = Math.random() * 20;
			circles[i] = new Circle(x, y, r);
		}
		return circles;
	}

	public static void outputCircles(Circle[] circles) {
		for (Circle circle : circles) {
			System.out.println(circle.getCircle());
		}
	}

	public static Circle[] getIsolatedCircles(Circle[] circles) {
		Circle[] temp = new Circle[circles.length];
		int numberOfIsolatedCircles = 0;
		for (int i = 0; i < circles.length - 1; i++) {
			boolean isIsolated = true;
			for (int j = i + 1; j < circles.length; j++) {
				if (circles[i].isIntersected(circles[j]) 
						|| circles[i].contains(circles[j])
						|| circles[j].contains(circles[i])) {
					isIsolated = false;
					break;
				}
			}
			if (isIsolated) {
				temp[numberOfIsolatedCircles++] = circles[i];
			}
		}
		Circle[] result = null;
		if (numberOfIsolatedCircles > 0) {
			result = new Circle[numberOfIsolatedCircles];
			System.arraycopy(temp, 0, result, 0, numberOfIsolatedCircles);
		}
		return result;
	}
}
