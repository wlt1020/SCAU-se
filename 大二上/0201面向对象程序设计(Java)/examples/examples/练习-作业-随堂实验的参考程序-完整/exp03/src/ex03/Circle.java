package ex03;

public class Circle {
	private Point center;
	private double radius;

	// constructors-----------------------------
	public Circle() {
		this(0, 0, 1.0);
	}

	public Circle(int x, int y, double radius) {
		this(new Point(x, y), radius);
	}

	public Circle(Point center, double radius) {
		this.center = center;
		this.radius = radius;
	}

	// functions--------------------------------
	public double getArea() {
		return Math.PI * radius * radius;
	}

	public boolean isIntersected(Circle other) {
		double distanceOfTwoCenter = this.center.distance(other.center);
		double sumOfTwoRadius = this.radius + other.radius;
		double diffOfTwoRadius = Math.abs(this.radius - other.radius);
		return distanceOfTwoCenter < sumOfTwoRadius && distanceOfTwoCenter > diffOfTwoRadius;
	}

	public boolean contains(Circle other) {
		boolean result = false;
		double distanceOfTwoCenter = this.center.distance(other.center);
		double diffOfTwoRadius = this.radius - other.radius;
		if (diffOfTwoRadius > 0 && distanceOfTwoCenter < diffOfTwoRadius) {
			result = true;
		}
		return result;
	}

	public String getCircle() {
		return String.format("[Circle(%3d, %3d)-%5.2f]", center.getX(), center.getY(), radius);
	}

	// setter && getter-------------------------
	public Point getCenter() {
		return center;
	}

	public void setCenter(Point center) {
		this.center = center;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

}
