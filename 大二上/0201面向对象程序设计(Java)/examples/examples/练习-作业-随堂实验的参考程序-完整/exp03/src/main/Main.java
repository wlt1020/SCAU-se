package main;

import java.util.Scanner;

import ex03.Circle;
import ex03.Circles;

public class Main {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("input number of circles: ");
		int n = input.nextInt();

		Circle[] circles = Circles.createCircles(n);
		System.out.printf("生成的%d个圆如下：\n", n);
		Circles.outputCircles(circles);

		Circle[] isolatedCirles = Circles.getIsolatedCircles(circles);
		if (isolatedCirles == null) {
			System.out.println("无孤立圆");
		} else {
			System.out.printf("共有%d个孤立圆如下：\n", isolatedCirles.length);
			Circles.outputCircles(isolatedCirles);
		}
	}

}
