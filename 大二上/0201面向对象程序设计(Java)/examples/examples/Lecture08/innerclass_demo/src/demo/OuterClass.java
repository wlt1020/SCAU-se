package demo;

public class OuterClass {
    private static int staticDataOuterClass = 10;
    private int instanceDataOuterClass = 10;
    private int data = 1000;
    
    private class InnerClass {
        private int data = 10;
        public void test() {
            System.out.println(data);
            System.out.println(OuterClass.this.data);
            System.out.println(staticDataOuterClass);
            System.out.println(instanceDataOuterClass);
        }
    }
    
    private static class StaticNestedClass {
        private static int NUMBER = 100;
        private int data = 1000;
        public void test() {
            System.out.println(data);
            System.out.println(staticDataOuterClass);
            //System.out.println(instanceDataOuterClass);
        }
    }
    
    public void test() {
        System.out.println("静态内部类使用:");
        System.out.println(StaticNestedClass.NUMBER);
        StaticNestedClass a = new StaticNestedClass();
        System.out.println(a.data);
        
        System.out.println("实例内部类使用");
        InnerClass b = new InnerClass();
        System.out.println(b.data);
    }
}
