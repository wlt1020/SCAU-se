package demo;

public abstract class Greeter {
    protected String name;

    public Greeter(String name) {
        this.name = name;
    }
    
    public abstract void sayHi();
}
