package demo;

public class Demo {
    public static void main(String[] args) {
        Greeter first = new Greeter("Tom") {
            @Override
            public void sayHi() {
                System.out.println("Hello " + name);
            }
        };
        
        first.sayHi();
        
        Greeting second = new Greeting() {
            
            @Override
            public void greet() {
                System.out.println("Hello World!");
            }
        };
        
        second.greet();
    }

}
