package demo;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Circle c = null;
		assert false;
		while (true) {
			try {
				System.out.print("输入半径：");
				double r = input.nextDouble();
				c = new Circle(r);
				break;
			} catch (InputMismatchException ex) {
				System.out.println("输入的数值不是合法实数！");
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
			input.nextLine();
		}
		System.out.println(c.getArea());
	}

}
