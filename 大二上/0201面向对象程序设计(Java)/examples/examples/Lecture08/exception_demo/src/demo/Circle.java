package demo;

public class Circle {
	private double radius;

	public Circle(double radius) throws Exception{
		super();
		if (radius <= 0) {
			throw new Exception("半径的值必须大于0！ radius = " + radius);
		}
		this.radius = radius;
	}

	public double getArea() {
		return Math.PI * radius * radius;
	}
	
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) throws Exception {
		if (radius <= 0) {
			throw new Exception("半径的值必须大于0！ radius = " + radius);
		}
		this.radius = radius;
	}

}
