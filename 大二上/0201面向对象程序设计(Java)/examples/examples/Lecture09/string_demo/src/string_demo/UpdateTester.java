package string_demo;

public class UpdateTester {

	public static void main(String[] args) {
		String s1 = "Welcome to Java";
		String s2 = s1.replaceAll("Java", "C++");

		System.out.println("s1 = " + s1);
		System.out.println("s2 = " + s2);
	}

}
