package regex_demo;

import java.util.Arrays;
import java.util.Scanner;

public class Tester {

    public static void main(String[] args) {
        String src = "java vb vc++ visual";
        
        String s1 = src.replaceAll("v\\w*", "==");
        String s2 = src.replaceFirst("v\\w*", "==");
        
        System.out.println(s1);
        System.out.println(s2);
        
        String[] a1 = "oboo:and:foo".split("o", -1);
        String[] a2 = "oboo:and:foo".split("o", 0);
        System.out.println(Arrays.asList(a1));
        System.out.println(Arrays.asList(a2));
        
        String regexForNatrualNumber = "[1-9][\\d]*"; //"[1-9][0-9]*";
        String regexForDecimal = "[+-]{0,1}([1-9][0-9]*|[0])\\.[0-9]*";
        

        Scanner input = new Scanner(System.in);

        String s = input.next();

        if (s.matches(regexForDecimal)) {
            System.out.println("输入正确");
        } else {
            System.out.println("输入错误");
        }
    }

}
