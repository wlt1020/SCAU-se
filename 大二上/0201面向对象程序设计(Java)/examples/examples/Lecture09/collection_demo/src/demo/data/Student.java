package demo.data;

public class Student extends Person {
    private String school;

    // ----------------------------------------------------
    public Student(String name, String school) {
        super(name);
        this.school = school;
    }

    // ----------------------------------------------------
    @Override
    public String sayHi() {
        return super.sayHi() + " , I am from " + school;
    }

    // ----------------------------------------------------
    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }
}
