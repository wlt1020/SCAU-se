package demo.list;

import java.util.ArrayList;

import demo.data.Person;
import demo.data.Student;
import demo.data.Teacher;

public class ListTester {

    public static void main(String[] args) {
        ArrayList<Person> list = new ArrayList<>();
        
        Student s = new Student("张三", "华南农业大学");
        Teacher t = new Teacher("李四", "程序设计");
        
        list.add(s);
        list.add(t);
        
        for(Person p : list) {
            System.out.println(p.sayHi());
        }
        
        Student stud = new Student("张三", "华南理工大学");
        System.out.println(list.contains(stud));
        
        System.out.println(list.size());
    }

}
