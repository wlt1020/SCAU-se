package demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class TraditionDemo {

    public static void copy(File src, File tar) {
        FileInputStream srcStream = null;
        FileOutputStream desStream = null;
        try {
            srcStream = new FileInputStream(src);
            desStream = new FileOutputStream(tar);
            int data;
            while((data = srcStream.read())!=-1) {
                desStream.write(data);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(desStream!=null) {
                try {
                    desStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(srcStream!=null) {
                try {
                    srcStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }            
        }
    }
    
    public static void copyAutoClose(File src, File tar) {
        try(FileInputStream srcStream = new FileInputStream(src);
            FileOutputStream desStream = new FileOutputStream(tar)) {            
            int data;
            while((data = srcStream.read())!=-1) {
                desStream.write(data);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } 
    }
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input Source File Name:");
        String sourceName = input.nextLine();
        System.out.print("Input Target File Name:");
        String targetName = input.nextLine();
        
        File sourceFile = new File(sourceName);
        File targetFile = new File(targetName);
        
        if(!sourceFile.exists() || !sourceFile.isFile()) {
            System.out.println("Source file name is not existed or not a file!");
            return;
        }

        if(targetFile.exists()) {
            System.out.println("Target file name is existed!");
            return;
        }
        
        copy(sourceFile, targetFile);
        
        System.out.println("File copy finised!");
    }

}
