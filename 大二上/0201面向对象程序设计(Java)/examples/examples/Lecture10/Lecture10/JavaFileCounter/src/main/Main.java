package main;

import java.io.File;
import java.util.Scanner;

import counter.JavaSourceCounter;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("A folder name: ");
        String folderName = input.nextLine();

        File folder = new File(folderName);

        if (folder.isDirectory()) {
            JavaSourceCounter counter = new JavaSourceCounter();
            counter.parse(folder);

            System.out.println(counter.getFileDetails());
            System.out.printf(
                    "%d Java files, total size: %d\n", 
                    counter.getNumberOfJavaFiles(),
                    counter.getTotalSizeOfJavaFiles());
        } else {
            System.out.println("It's not a directory!");
        }

        input.close();
    }
}
