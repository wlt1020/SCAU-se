package counter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JavaSourceCounter {
    private int numberOfJavaFiles;
    private long totalSizeOfJavaFiles;
    private String fileDetails;

    public void parse(File folder) {
        this.numberOfJavaFiles = 0;
        this.totalSizeOfJavaFiles = 0;
        
        StringBuilder builder = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        File[] items = folder.listFiles();
        for (File item : items) {
            if (item.isFile() && item.getName().toLowerCase().endsWith(".java")) {
                this.numberOfJavaFiles++;
                this.totalSizeOfJavaFiles += item.length();
                String time = sdf.format(new Date(item.lastModified()));
                builder.append(
                        String.format("%-30s %6d bytes %s", item.getName(), item.length(), time)
                );
                builder.append('\n');
            }
        }
        this.fileDetails = builder.toString();
    }

    public int getNumberOfJavaFiles() {
        return numberOfJavaFiles;
    }

    public long getTotalSizeOfJavaFiles() {
        return totalSizeOfJavaFiles;
    }

    public String getFileDetails() {
        return fileDetails;
    }

}
