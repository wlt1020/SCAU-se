package counter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CodeLineCounter {
	private int numberOfLines;
	private int numberOfWord;

	public void count(File file, String word) {
		this.numberOfLines = 0;
		this.numberOfWord = 0;

		Pattern pattern = Pattern.compile("[\\W]" + word + "[\\W]");
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String line = null;
			// readLine()方法返回null，表示读取结束
			while ((line = reader.readLine()) != null) {
				this.numberOfLines++;
				Matcher matcher = pattern.matcher(" " + line);
				while (matcher.find()) {
					this.numberOfWord++;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getNumberOfLines() {
		return numberOfLines;
	}

	public int getNumberOfWord() {
		return numberOfWord;
	}

}
