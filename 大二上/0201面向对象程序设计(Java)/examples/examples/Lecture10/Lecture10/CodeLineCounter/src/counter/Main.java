package counter;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		String filename = "src/counter/CodeLineCounter.java";
		File file = new File(filename);
		String word = "int";

		CodeLineCounter counter = new CodeLineCounter();
		counter.count(file, word);

		System.out.printf("文件共%d行, 其中[%s]出现了%d次！", 
				counter.getNumberOfLines(),
				word,
				counter.getNumberOfWord());
	}

}
