package main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

import data.Student;

public class Main {
	public static void main(String[] args) {
		Student one = new Student("张三", 67.5, new Date());
		Student[] students = {
				new Student("张三", 67.5, new Date()),
				new Student("李四", 57.5, new Date(100000000000L)),
				new Student("王五", 66.5, new Date(200000)),
		};
		writeStudentArray(students, "student.dat");
		readStudentArray("student.dat");
	}

	public static void writeFields(Student student, String filename) {
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
			oos.writeUTF(student.getName());
			oos.writeDouble(student.getWeight());
			oos.writeObject(student.getBirthday());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void readFields(String filename) {
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
			String name = ois.readUTF();
			double weight = ois.readDouble();
			Date birthday = (Date) ois.readObject();
			System.out.println(name + ", " + weight + "," + birthday);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) { // readObject()方法抛出的异常
			e.printStackTrace();
		}
	}

	/**
	 * Student类需要实现 java.io.Serializable
	 */
	public static void writeOneStudent(Student student, String filename) {
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
			oos.writeObject(student);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void readOneStudent(String filename) {
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
			Student student = (Student) ois.readObject();
			System.out.println(student);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) { // readObject()方法抛出的异常
			e.printStackTrace();
		}
	}

	public static void writeStudentArray(Student[] students, String filename) {
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
			oos.writeObject(students);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void readStudentArray(String filename) {
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
			Student[] students = (Student[]) ois.readObject();
			for (Student student : students) {
				System.out.println(student);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) { // readObject()方法抛出的异常
			e.printStackTrace();
		}
	}

}
