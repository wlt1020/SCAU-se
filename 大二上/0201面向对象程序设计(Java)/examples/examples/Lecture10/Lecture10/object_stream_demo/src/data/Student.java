package data;

import java.util.Date;

public class Student implements java.io.Serializable {
	private String name;
	private double weight;
	private Date birthday;
	
	public Student(String name, double weight, Date birthday) {
		super();
		this.name = name;
		this.weight = weight;
		this.birthday = birthday;
	}
	
	@Override
	public String toString() {
		return "Student [name=" + name + ", weight=" + weight + ", birthday=" + birthday + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
}
