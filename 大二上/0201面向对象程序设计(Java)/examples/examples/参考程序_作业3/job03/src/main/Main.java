package main;

import shape.Rectangle;
import shape.Utility;

public class Main {
	public static void main(String[] args) {
		Rectangle.setColor("RED"); //设置矩形颜色
		
		Rectangle[] rectangles = new Rectangle[10];
		
		for(int i=0; i<rectangles.length; i++) {
			double w = Math.random()*100;
			double h = Math.random()*100;
			rectangles[i] = new Rectangle(w, h);
		}
		
		System.out.println("输出所有矩形对象：");
		Utility.output(rectangles);
		
		Utility.sort(rectangles);
		
		System.out.println("输出排序后所有矩形对象：");
		Utility.output(rectangles);	
		
	}
}
