package demo;

public class Circle {
	double radius;
	
	Circle(){
		radius = 1.0;
	}
	
	Circle(double r) {
		radius = r;
	}
	
	double getArea() {
		return Math.PI * radius * radius;
	}
}
