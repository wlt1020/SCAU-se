package demo;

public class Cone {
	Circle bottom;
	double height;
	
	// 由整体对象负责创建部分对象
	Cone() {
		bottom = new Circle();
		height = 1.0;
	}
	
	// 由整体对象负责创建部分对象
	Cone(double r, double h) {
		bottom = new Circle(r);
		height = h;
	}

	// 部分对象已经存在，整体对象负责装配
	Cone(Circle b, double h) {
		bottom = b;
		height = h;
	}
	
	double getVolume() {
		return bottom.getArea() * height / 3.0;
	}
}
