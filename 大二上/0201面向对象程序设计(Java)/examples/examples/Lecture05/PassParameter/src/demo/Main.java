package demo;

public class Main {

    public static void main(String[] args) {
        BasicDataDemo basic = new BasicDataDemo();
        int x = 10, y = 100;
        System.out.println("初始情况：" + x + " , " + y);
        basic.swapInteger(x, y);
        System.out.println("调用swapInteger后：" + x + " , " + y);
        basic.swapDouble(x, y);
        System.out.println("调用swapDouble后：" + x + " , " + y);
        //演示不能向swapInteger方法传递double参数
        
        //---------------------------------------------------
        CircleDemo demo = new CircleDemo();
        Circle first = new Circle(10.0), second = new Circle(20.0);
        demo.swapCircle(first, second);
        System.out.println(first + " , " + second);
        System.out.println(first.radius + " , " + second.radius);
        

    }

}
