package demo;

public class BasicDataDemo {
    void swapInteger(int a, int b) {
        int temp = a;
        a = b;
        b = temp;
    }
    
    void swapDouble(double a, double b) {
        double temp = a;
        a = b;
        b = temp;
    }
}
