package demo;

public class Circle {
    double radius;
    Circle(double r) {
        radius = r;
    }
    double getArea() {
        return Math.PI * radius * radius;
    }
}
