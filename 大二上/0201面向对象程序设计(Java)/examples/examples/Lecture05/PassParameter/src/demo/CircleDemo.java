package demo;

public class CircleDemo {
    void swapCircle(Circle c1, Circle c2) {
        Circle temp = c1;    
    	c1 = c2;
        c2 = temp;
    }
    
    void swapRadiusOfCircle(Circle c1, Circle c2) {
        double tempRadius = c1.radius;
        c1.radius = c2.radius;
        c2.radius = tempRadius;
    }
}
