package demo01;

public class Lader {
    float above;  // 梯形上底
    float bottom; // 梯形下底
    float height; // 梯形的高
    float area;   // 梯形的面积

    float getArea() {
        return (above + bottom) * height / 2.0F;
    }

    void setHeight(float newHeight) {
        height = newHeight;
    }
}
