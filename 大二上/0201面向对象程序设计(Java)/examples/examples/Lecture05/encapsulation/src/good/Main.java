package good;

import java.util.Scanner;

public class Main {
    public Student[] createStudents(int numberOfStudents) {
        Scanner input = new Scanner(System.in);
        Student[] students = new Student[numberOfStudents];

        for (int i = 0; i < students.length; i++) {
            System.out.printf("输入第%d个学生的学号和姓名: ", i + 1);
            int id = input.nextInt();
            String name = input.nextLine();
            students[i] = new Student(id, name);
        }

        return students;
    }

    public void printStudents(Student[] students) {
        for (Student student : students) {
            student.print();
        }
    }

    public static void main(String[] args) {
        Main main = new Main();
        Student[] students = main.createStudents(3);
        main.printStudents(students);
    }
}
