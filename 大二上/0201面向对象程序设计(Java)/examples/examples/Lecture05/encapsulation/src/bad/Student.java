package bad;

public class Student {
    public int id;
    public String name;
    
    public Student(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void print() {
        System.out.println(id + ", " + name);
    }
}
