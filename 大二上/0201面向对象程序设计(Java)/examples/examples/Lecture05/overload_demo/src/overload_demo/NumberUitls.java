package overload_demo;

public class NumberUitls {
    public static int max(int a, int b) {
    	System.out.println("a");
    	return a > b ? a : b;
    }

    public static double max(double a, double b) {
    	System.out.println("b");
    	return a > b ? a : b;
    }
    public static double max(float a, float b) {
    	System.out.println("c");
    	return a > b ? a : b;
    }

    public static int max(int a, int b, int c) {
    	System.out.println("d");
    	return a > b ? a : b > c ? b : c;
    }
}
