//Main.java
package demo02;

public class Main {

    public static void main(String[] args) {
        Lader one = new Lader(10, 20, 5);
        System.out.println("梯形one的面积：" + one.getArea());
        one.height = 10.0F; // 直接修改梯形one的高度
        System.out.println("梯形one的面积：" + one.getArea());
        if (one != null) {
            one.setHeight(20.0F); // 调用one的方法修改梯形one的高度
        }
        System.out.println("梯形one的面积：" + one.getArea());
    }

}
