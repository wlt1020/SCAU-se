//Lader.java
package demo02;

public class Lader {
    float above;  // 梯形上底
    float bottom; // 梯形下底
    float height; // 梯形的高

    // 无参构造方法
    Lader() {
    }

    // 有参构造方法
    Lader(float a, float b, float h) {
        above = a;
        bottom = b;
        height = h;
    }

    float getArea() {
        return (above + bottom) * height / 2.0F;
    }

    void setHeight(float newHeight) {
        height = newHeight;
    }
}
