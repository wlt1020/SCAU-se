package demo;

public class Rectangle {
    double width;
    double height;

    public Rectangle(double width, double height) {
        this.width = width; // this必须使用, 引用正在创建的对象
        this.height = height;
    }

    public Rectangle() {
        this(1.0, 1.0); // 调用本类的其他构造方法
    }

    public void setWidth(double width) {
        this.width = width;// this必须使用
    }

    public double getArea() {
        return this.width * this.height; //this可以省略
    }
}
