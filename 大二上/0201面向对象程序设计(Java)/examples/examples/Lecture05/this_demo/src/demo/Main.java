package demo;

public class Main {

    public static void main(String[] args) {
        Rectangle r1 = new Rectangle(10.0, 10.0);
        Rectangle r2 = new Rectangle(20.0, 20.0);
        
        r1.setWidth(100.0);
        r2.setWidth(200.0);
    }

}
