package demo;

import java.awt.Rectangle;

public class Main {
    public static void main(String[] args) {
        StaticDemo d1 = null, d2 = null; // 此时类并没有加载到内存

        // 1. 对静态变量的访问
        System.out.println("通过d1访问staticData: " + d1.staticData); // 首次主动访问类时，类被加载
        System.out.println("通过d2访问staticData: " + d2.staticData);
        System.out.println("通过[类名]访问staticData: " + StaticDemo.staticData);

        StaticDemo.staticData = 100; // 通过任何一种方式都可以访问类变量

        System.out.println("通过d1访问staticData: " + d1.staticData); // 首次主动访问类时，类被加载
        System.out.println("通过d2访问staticData: " + d2.staticData);
        System.out.println("通过[类名]访问staticData: " + StaticDemo.staticData);

        // 2. 对实例变量的访问
        // System.out.println("d1.instanceData = " + d1.instanceData); //出错

        d1 = new StaticDemo();
        d2 = new StaticDemo();
        d1.instanceData = 11;
        d2.instanceData = 22;

        System.out.println("d1.instanceData = " + d1.instanceData);
        System.out.println("d2.instanceData = " + d2.instanceData);
    }
}
