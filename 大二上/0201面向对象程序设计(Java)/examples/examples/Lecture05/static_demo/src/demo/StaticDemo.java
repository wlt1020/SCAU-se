package demo;
/**
 * 实例成员与静态成员示例
 * 仅用于演示语法和使用
 * @author xiaolei
 *
 */
public class StaticDemo {
    int        instanceData; // 实例数据域
    static int staticData;   // 静态数据域

    int getInstanceData() {
        return instanceData;
    }

    static int getStaticData() {
        return staticData;
    }
}
